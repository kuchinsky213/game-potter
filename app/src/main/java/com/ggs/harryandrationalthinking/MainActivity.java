package com.ggs.harryandrationalthinking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ggs.harryandrationalthinking.levels.Level1_1;
import com.ggs.harryandrationalthinking.levels.Level3.Level3;
import com.ggs.harryandrationalthinking.levels.Level3.Preview_lvl3;
import com.ggs.harryandrationalthinking.levels.Level4.Level4;
import com.ggs.harryandrationalthinking.levels.Level4.Preview_Lvl4;
import com.ggs.harryandrationalthinking.levels.Level5.Level5;
import com.ggs.harryandrationalthinking.levels.Level5.Preview_lvl5;
import com.ggs.harryandrationalthinking.levels.level1;
import com.ggs.harryandrationalthinking.levels.level2.Level2;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
Button exit, newG, conG;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences save = getSharedPreferences("Save", MODE_PRIVATE);
final int level = save.getInt("Level", 0);
//        editor.putInt("Level", 0);

        exit=findViewById(R.id.exit);
        conG=findViewById(R.id.conG);
        newG=findViewById(R.id.newG);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }

        });

        newG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SharedPreferences save = getSharedPreferences("Save", MODE_PRIVATE);
                    SharedPreferences.Editor editor = save.edit();
                    editor.putInt("Level", 0);
                    editor.commit();
                    Intent l1= new Intent(MainActivity.this, level1.class);
                    startActivity(l1);
                    finish();
                }
                catch (Exception e){

                }

            }
        });


        conG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               switch (level){
                   case 0:
                       try {
                           Intent l1= new Intent(MainActivity.this, level1.class);startActivity(l1);finish();
                       }catch (Exception e){

                       }break;
                   case 1:
                       try {
                           Intent l1_1= new Intent(MainActivity.this, Level1_1.class);startActivity(l1_1);finish();
                       }catch (Exception e){

                       }break;
                   case 2:
                       try {
                           Intent l2= new Intent(MainActivity.this, Level2.class);startActivity(l2);finish();
                       }catch (Exception e){

                       }break;
                   case 3:
                       try {
                           Intent l3= new Intent(MainActivity.this, Level3.class);startActivity(l3);finish();
                       }catch (Exception e){

                       }break;
                   case 4:
                       try {
                           Intent l4= new Intent(MainActivity.this, Level4.class);startActivity(l4);finish();
                       }catch (Exception e){

                       }break;
                   case 5:
                       try {
                           Intent l5= new Intent(MainActivity.this, Level5.class);startActivity(l5);finish();
                       }catch (Exception e){

                       }break;
                   default:break;
               }
            }
        });
        switch (level){
            case 0:
                conG.setVisibility(View.GONE);

                break;
            default:
                newG.setText(R.string.repeat);
                break;

        }

    }
}