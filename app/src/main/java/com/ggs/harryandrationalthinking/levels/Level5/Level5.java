package com.ggs.harryandrationalthinking.levels.Level5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.ggs.harryandrationalthinking.R;
import com.ggs.harryandrationalthinking.data.FiveTable;
import com.ggs.harryandrationalthinking.levels.Level3.Level3;
import com.ggs.harryandrationalthinking.levels.Level4.Preview_Lvl4;

public class Level5 extends AppCompatActivity {
    Delay delay = new Delay();
    TextView l2_1;
    TextView l2_2;
    TextView l2_3;
    TextView l2_4;
    TextView l2_5;
    Button b1l2;
    Button b2l2;
    TextView l2_6;
    TextView l2_7;
    TextView l2_8;
    TextView l2_9;
    TextView l2_10;
    TextView l2_11;
    TextView l2_12;
    TextView l2_13;
    TextView l2_14;
    TextView l2_15;
    TextView l2_16;
    TextView l2_17;
    TextView l2_18;
    TextView l2_19;
    TextView l2_20;
    TextView l2_21;
    TextView l2_22;
    TextView l2_23;
    TextView l2_24;

    TextView l2_25;
    TextView l2_26;
    TextView l2_27;
    TextView l2_28;
    TextView l2_29;
    TextView l2_30;
    TextView l2_31;
    TextView l2_32;
    TextView l2_33;
    TextView l2_34;
    TextView l2_35;
    TextView l2_36;
    TextView l2_37;
    TextView l2_38;
    TextView l2_39;
    TextView l2_40;
    TextView l2_41;
    TextView l2_42;
    TextView l2_43;
    Button b3l2;
    Button b4l2;
    TextView l2_44;
    TextView l2_45;
    TextView l2_46;
    TextView l2_47;
    TextView l2_48;
    TextView l2_49;
    TextView l2_50;
    TextView l2_51;
    TextView l2_52;
    TextView l2_53;
    TextView l2_54;
    TextView l2_55;
    TextView l2_56;
    TextView l2_57;
    TextView l2_58;
    TextView l2_59;
    TextView l2_60;
    TextView l2_61;
    TextView l2_62;
    TextView l2_63;
    TextView l2_64;
    TextView l2_65;
    TextView l2_66;
    TextView l2_67;
    TextView l2_68;
    TextView l2_69;
    TextView l2_70;
    TextView l2_71;
    TextView l2_72;
    TextView l2_73;
    TextView l2_74;
    TextView l2_75;
    TextView l2_76;
    TextView l2_77;
    TextView l2_78;
    TextView l2_79;
    TextView l2_80;
    TextView l2_81;
    TextView l2_82;
    TextView l2_83;
    TextView l2_84;
    TextView l2_85;
    TextView l2_86;
    TextView l2_87;
    TextView l2_88;
    TextView l2_89;
    TextView l2_90;
    TextView l2_91;
    TextView l2_92;
    TextView l2_93;
    TextView l2_94;
    TextView l2_95;
    TextView l2_96;
    TextView l2_97;
    TextView l2_98;
    TextView l2_99;
    TextView l2_100;
    TextView l2_101;
    TextView l2_102;
    TextView l2_103;
    TextView l2_104;
    TextView l2_105;
    TextView l2_106;
    TextView l2_107;
    TextView l2_108;
    TextView l2_109;
    TextView l2_110;
    TextView l2_111;
    TextView l2_112;
    TextView l2_113;
    TextView l2_114;
    TextView l2_115;
    TextView l2_116;
    TextView l2_117;
    TextView l2_118;
    TextView l2_119;
    TextView l2_120;
    TextView l2_121;
    TextView l2_122;
    TextView l2_123;
    TextView l2_124;
    TextView l2_125;
    TextView l2_126;
    TextView l2_127;
    TextView l2_128;
    TextView l2_129;
    TextView l2_48_v;
FiveTable fiveTable = new FiveTable();
    Button    b5l2;
    public Animation a;
    public int line = -1;
    int counterFirstLine = 0;
    int counterSecondLine = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level5);
        SharedPreferences save = getSharedPreferences("Save", MODE_PRIVATE);
        SharedPreferences.Editor editor = save.edit();
        editor.putInt("Level", 5);
        editor.commit();

        final TextView l2_1 = findViewById(R.id.l5_1);
        final     TextView l2_2 = findViewById(R.id.l5_2);
        final    TextView l2_3 = findViewById(R.id.l5_3);
        final    TextView  l2_4 = findViewById(R.id.l5_4);
        final    TextView l2_5  = findViewById(R.id.l5_5);

        final  TextView l2_6  = findViewById(R.id.l5_6);
        final  TextView l2_7  = findViewById(R.id.l5_7);
        final  TextView l2_8  = findViewById(R.id.l5_8);
        final  TextView l2_9  = findViewById(R.id.l5_9);
        final TextView  l2_10 = findViewById(R.id.l5_10);
        final  TextView l2_11  = findViewById(R.id.l5_11);
        final  TextView l2_12  = findViewById(R.id.l5_12);
        final  TextView l2_13  = findViewById(R.id.l5_13);
        final  TextView l2_14  = findViewById(R.id.l5_14);
        final  TextView l2_15  = findViewById(R.id.l5_15);
        final  TextView l2_16  = findViewById(R.id.l5_16);
        final TextView l2_17  = findViewById(R.id.l5_17);
        final TextView l2_18  = findViewById(R.id.l5_18);
        final TextView l2_19  = findViewById(R.id.l5_19);
        final TextView l2_20  = findViewById(R.id.l5_20);
        final TextView  l2_21 = findViewById(R.id.l5_21);
        final  TextView l2_22  = findViewById(R.id.l5_22);
        final   TextView l2_23  = findViewById(R.id.l5_23);
        final  TextView l2_24  = findViewById(R.id.l5_24);

        final TextView l2_25  = findViewById(R.id.l5_25);
        final  TextView l2_26  = findViewById(R.id.l5_26);
        final  TextView l2_27  = findViewById(R.id.l5_27);
        final  TextView l2_28  = findViewById(R.id.l5_28);
        final  TextView l2_29  = findViewById(R.id.l5_29);
        final  TextView l2_30  = findViewById(R.id.l5_30);
        final  TextView l2_31  = findViewById(R.id.l5_31);
        final  TextView l2_32  = findViewById(R.id.l5_32);
        final TextView l2_33  = findViewById(R.id.l5_33);
        final TextView l2_34  = findViewById(R.id.l5_34);
        final TextView l2_35  = findViewById(R.id.l5_35);
        final  TextView l2_36  = findViewById(R.id.l5_36);
        final TextView l2_37  = findViewById(R.id.l5_37);
        final TextView l2_38  = findViewById(R.id.l5_38);
        final TextView l2_39  = findViewById(R.id.l5_39);
        final TextView l2_40  = findViewById(R.id.l5_40);
        final TextView l2_41  = findViewById(R.id.l5_41);
        final TextView l2_42  = findViewById(R.id.l5_42);
        final  TextView l2_43  = findViewById(R.id.l5_43);

        final  TextView l2_44  = findViewById(R.id.l5_44);

        final   TextView l2_45  = findViewById(R.id.l5_45);
        final   TextView l2_46  = findViewById(R.id.l5_46);
        final  TextView l2_47  = findViewById(R.id.l5_47);
        final  TextView l2_48  = findViewById(R.id.l5_48);
        final  TextView l2_48_v  = findViewById(R.id.l5_48_v);



        final   Button b1l2  = findViewById(R.id.b1l5);
        final  Button b2l2  = findViewById(R.id.b2l5);




        final  TextView l2_49  = findViewById(R.id.l5_49);
        final  TextView l2_50  = findViewById(R.id.l5_50);
        final  TextView l2_51  = findViewById(R.id.l5_51);
        final  TextView l2_52  = findViewById(R.id.l5_52);
        final   TextView l2_53  = findViewById(R.id.l5_53);
        final   TextView l2_54  = findViewById(R.id.l5_54);
        final  TextView l2_55 = findViewById(R.id.l5_55);
        final  TextView l2_56 = findViewById(R.id.l5_56);
        final TextView l2_57 = findViewById(R.id.l5_57);
        final TextView l2_58 = findViewById(R.id.l5_58);
        final TextView l2_59  = findViewById(R.id.l5_59);
        final  TextView l2_60  = findViewById(R.id.l5_60);
        final  TextView l2_61  = findViewById(R.id.l5_61);
        final TextView l2_62  = findViewById(R.id.l5_62);
        final  TextView l2_63  = findViewById(R.id.l5_63);
        final  TextView l2_64  = findViewById(R.id.l5_64);
        final  TextView l2_65  = findViewById(R.id.l5_65);
        final  TextView l2_66  = findViewById(R.id.l5_66);
        final  TextView l2_67  = findViewById(R.id.l5_67);





        final  Button b3l2  = findViewById(R.id.b3l5);
        final  Button b4l2  = findViewById(R.id.b4l5);


        TextView l2_68= findViewById(R.id.l5_68);


        TextView l2_69= findViewById(R.id.l5_69);
        TextView l2_70= findViewById(R.id.l5_70);
        TextView l2_71= findViewById(R.id.l5_71);
        TextView l2_72= findViewById(R.id.l5_72);
        TextView l2_73= findViewById(R.id.l5_73);
        TextView l2_74= findViewById(R.id.l5_74);
        TextView l2_75= findViewById(R.id.l5_75);
        TextView l2_76= findViewById(R.id.l5_76);
        TextView l2_77= findViewById(R.id.l5_77);
        TextView l2_78= findViewById(R.id.l5_78);
        TextView l2_79= findViewById(R.id.l5_79);
        TextView l2_80= findViewById(R.id.l5_80);
        TextView l2_81= findViewById(R.id.l5_81);
        TextView l2_82= findViewById(R.id.l5_82);
        TextView l2_83= findViewById(R.id.l5_83);
        TextView l2_84= findViewById(R.id.l5_84);
        TextView l2_85= findViewById(R.id.l5_85);
        TextView l2_86= findViewById(R.id.l5_86);
        TextView l2_87= findViewById(R.id.l5_87);
        TextView l2_88= findViewById(R.id.l5_88);
        TextView l2_89= findViewById(R.id.l5_89);
        TextView l2_90= findViewById(R.id.l5_90);
        TextView l2_91= findViewById(R.id.l5_91);
        TextView l2_92= findViewById(R.id.l5_92);
        TextView l2_93= findViewById(R.id.l5_93);
        TextView l2_94= findViewById(R.id.l5_94);
        TextView l2_95= findViewById(R.id.l5_95);
        TextView l2_96= findViewById(R.id.l5_96);
        TextView l2_97= findViewById(R.id.l5_97);
        TextView l2_98= findViewById(R.id.l5_98);
        TextView l2_99= findViewById(R.id.l5_99);
        TextView l2_100= findViewById(R.id.l5_100);
        TextView l2_101= findViewById(R.id.l5_101);
        TextView l2_102= findViewById(R.id.l5_102);
        TextView l2_103= findViewById(R.id.l5_103);
        TextView l2_104= findViewById(R.id.l5_104);
        TextView l2_105= findViewById(R.id.l5_105);
        TextView l2_106= findViewById(R.id.l5_106);
        TextView l2_107= findViewById(R.id.l5_107);
        TextView l2_108= findViewById(R.id.l5_108);
        TextView l2_109= findViewById(R.id.l5_109);
        TextView l2_110= findViewById(R.id.l5_110);
        TextView l2_111= findViewById(R.id.l5_111);
        TextView l2_112= findViewById(R.id.l5_112);
        TextView l2_113= findViewById(R.id.l5_113);
        TextView l2_114= findViewById(R.id.l5_114);
        TextView l2_115= findViewById(R.id.l5_115);
        TextView l2_116= findViewById(R.id.l5_116);
        TextView l2_117= findViewById(R.id.l5_117);
        TextView l2_118= findViewById(R.id.l5_118);
        TextView l2_119= findViewById(R.id.l5_119);
        TextView l2_120= findViewById(R.id.l5_120);
        TextView l2_121= findViewById(R.id.l5_121);
        TextView l2_122= findViewById(R.id.l5_122);
        TextView l2_123= findViewById(R.id.l5_123);
        TextView l2_124= findViewById(R.id.l5_124);
        TextView l2_125= findViewById(R.id.l5_125);
        TextView l2_126= findViewById(R.id.l5_126);
        TextView l2_127= findViewById(R.id.l5_127);
        TextView l2_128= findViewById(R.id.l5_128);
        TextView l2_129= findViewById(R.id.l5_129);
        final  Button    b5l2  = findViewById(R.id.b5l5);


        l2_1.setText(fiveTable.fiveScenario_ru[0]);
          l2_2.setText(fiveTable.fiveScenario_ru[1]);
        l2_3.setText(fiveTable.fiveScenario_ru[2]);
        l2_4.setText(fiveTable.fiveScenario_ru[3]);
        l2_5.setText(fiveTable.fiveScenario_ru[4]);
        l2_6.setText(fiveTable.fiveScenario_ru[5]);
        l2_7.setText(fiveTable.fiveScenario_ru[6]);
        l2_8.setText(fiveTable.fiveScenario_ru[7]);
        l2_9.setText(fiveTable.fiveScenario_ru[8]);
        l2_10.setText(fiveTable.fiveScenario_ru[9]);
        l2_11.setText(fiveTable.fiveScenario_ru[10]);
        l2_12.setText(fiveTable.fiveScenario_ru[11]);
        l2_13.setText(fiveTable.fiveScenario_ru[12]);
        l2_14.setText(fiveTable.fiveScenario_ru[13]);
        l2_15.setText(fiveTable.fiveScenario_ru[14]);
        l2_16.setText(fiveTable.fiveScenario_ru[15]);
        l2_17.setText(fiveTable.fiveScenario_ru[16]);
        l2_18.setText(fiveTable.fiveScenario_ru[17]);
        l2_19.setText(fiveTable.fiveScenario_ru[18]);
        l2_20.setText(fiveTable.fiveScenario_ru[19]);
        l2_21.setText(fiveTable.fiveScenario_ru[20]);
        l2_22.setText(fiveTable.fiveScenario_ru[21]);
        l2_23.setText(fiveTable.fiveScenario_ru[22]);
        l2_24.setText(fiveTable.fiveScenario_ru[23]);
        l2_25.setText(fiveTable.fiveScenario_ru[24]);
        l2_26.setText(fiveTable.fiveScenario_ru[25]);
        l2_27.setText(fiveTable.fiveScenario_ru[26]);
        l2_28.setText(fiveTable.fiveScenario_ru[27]);
        l2_29.setText(fiveTable.fiveScenario_ru[28]);
        l2_30.setText(fiveTable.fiveScenario_ru[29]);
        l2_31.setText(fiveTable.fiveScenario_ru[30]);
        l2_32.setText(fiveTable.fiveScenario_ru[31]);
        l2_33.setText(fiveTable.fiveScenario_ru[32]);
        l2_34.setText(fiveTable.fiveScenario_ru[33]);
        l2_35.setText(fiveTable.fiveScenario_ru[34]);
        l2_36.setText(fiveTable.fiveScenario_ru[35]);
        l2_37.setText(fiveTable.fiveScenario_ru[36]);
        l2_38.setText(fiveTable.fiveScenario_ru[37]);
        l2_39.setText(fiveTable.fiveScenario_ru[38]);
        l2_40.setText(fiveTable.fiveScenario_ru[39]);
        l2_41.setText(fiveTable.fiveScenario_ru[40]);
        l2_42.setText(fiveTable.fiveScenario_ru[41]);
        l2_43.setText(fiveTable.fiveScenario_ru[42]);
        l2_44.setText(fiveTable.fiveScenario_ru[43]);
        l2_45.setText(fiveTable.fiveScenario_ru[44]);
        l2_46.setText(fiveTable.fiveScenario_ru[45]);
        l2_47.setText(fiveTable.fiveScenario_ru[46]);
        l2_48.setText(fiveTable.fiveScenario_ru[47]);
        l2_48_v.setText(fiveTable.fiveScenario_ru[48]);

        l2_49.setText(fiveTable.fiveScenario_ru[49]);
        l2_50.setText(fiveTable.fiveScenario_ru[50]);
        l2_51.setText(fiveTable.fiveScenario_ru[51]);
        l2_52.setText(fiveTable.fiveScenario_ru[52]);
        l2_53.setText(fiveTable.fiveScenario_ru[53]);
        l2_54.setText(fiveTable.fiveScenario_ru[54]);
        l2_55.setText(fiveTable.fiveScenario_ru[55]);
        l2_56.setText(fiveTable.fiveScenario_ru[56]);
        l2_57.setText(fiveTable.fiveScenario_ru[57]);
        l2_58.setText(fiveTable.fiveScenario_ru[58]);
        l2_59.setText(fiveTable.fiveScenario_ru[59]);
        l2_60.setText(fiveTable.fiveScenario_ru[60]);
        l2_61.setText(fiveTable.fiveScenario_ru[61]);
        l2_62.setText(fiveTable.fiveScenario_ru[62]);
        l2_63.setText(fiveTable.fiveScenario_ru[63]);
        l2_64.setText(fiveTable.fiveScenario_ru[64]);
        l2_65.setText(fiveTable.fiveScenario_ru[65]);
        l2_66.setText(fiveTable.fiveScenario_ru[66]);
        l2_67.setText(fiveTable.fiveScenario_ru[67]);
        l2_68.setText(fiveTable.fiveScenario_ru[68]);
        l2_69.setText(fiveTable.fiveScenario_ru[69]);
        l2_70.setText(fiveTable.fiveScenario_ru[70]);
        l2_71.setText(fiveTable.fiveScenario_ru[71]);
        l2_72.setText(fiveTable.fiveScenario_ru[72]);
        l2_73.setText(fiveTable.fiveScenario_ru[73]);
        l2_74.setText(fiveTable.fiveScenario_ru[74]);
        l2_75.setText(fiveTable.fiveScenario_ru[75]);
        l2_76.setText(fiveTable.fiveScenario_ru[76]);
        l2_77.setText(fiveTable.fiveScenario_ru[77]);
        l2_78.setText(fiveTable.fiveScenario_ru[78]);
        l2_79.setText(fiveTable.fiveScenario_ru[79]);
        l2_80.setText(fiveTable.fiveScenario_ru[80]);
        l2_81.setText(fiveTable.fiveScenario_ru[81]);
        l2_82.setText(fiveTable.fiveScenario_ru[82]);
        l2_83.setText(fiveTable.fiveScenario_ru[83]);
        l2_84.setText(fiveTable.fiveScenario_ru[84]);
        l2_85.setText(fiveTable.fiveScenario_ru[85]);
        l2_86.setText(fiveTable.fiveScenario_ru[86]);
        l2_87.setText(fiveTable.fiveScenario_ru[87]);
        l2_88.setText(fiveTable.fiveScenario_ru[88]);
        l2_89.setText(fiveTable.fiveScenario_ru[89]);
        l2_90.setText(fiveTable.fiveScenario_ru[90]);
        l2_91.setText(fiveTable.fiveScenario_ru[91]);
        l2_92.setText(fiveTable.fiveScenario_ru[92]);
        l2_93.setText(fiveTable.fiveScenario_ru[93]);
        l2_94.setText(fiveTable.fiveScenario_ru[94]);
        l2_95.setText(fiveTable.fiveScenario_ru[95]);
        l2_96.setText(fiveTable.fiveScenario_ru[96]);
        l2_97.setText(fiveTable.fiveScenario_ru[97]);
        l2_98.setText(fiveTable.fiveScenario_ru[98]);
        l2_99.setText(fiveTable.fiveScenario_ru[99]);
        l2_100.setText(fiveTable.fiveScenario_ru[100]);
        l2_101.setText(fiveTable.fiveScenario_ru[101]);
        l2_102.setText(fiveTable.fiveScenario_ru[102]);
        l2_103.setText(fiveTable.fiveScenario_ru[103]);
        l2_104.setText(fiveTable.fiveScenario_ru[104]);
        l2_105.setText(fiveTable.fiveScenario_ru[105]);
        l2_106.setText(fiveTable.fiveScenario_ru[106]);
        l2_107.setText(fiveTable.fiveScenario_ru[107]);
        l2_108.setText(fiveTable.fiveScenario_ru[108]);
        l2_109.setText(fiveTable.fiveScenario_ru[109]);
        l2_110.setText(fiveTable.fiveScenario_ru[110]);
        l2_111.setText(fiveTable.fiveScenario_ru[111]);
        l2_112.setText(fiveTable.fiveScenario_ru[112]);
        l2_113.setText(fiveTable.fiveScenario_ru[113]);
        l2_114.setText(fiveTable.fiveScenario_ru[114]);
        l2_115.setText(fiveTable.fiveScenario_ru[115]);
        l2_116.setText(fiveTable.fiveScenario_ru[116]);
        l2_117.setText(fiveTable.fiveScenario_ru[117]);
        l2_118.setText(fiveTable.fiveScenario_ru[118]);
        l2_119.setText(fiveTable.fiveScenario_ru[119]);
        l2_120.setText(fiveTable.fiveScenario_ru[120]);
        l2_121.setText(fiveTable.fiveScenario_ru[121]);
        l2_122.setText(fiveTable.fiveScenario_ru[122]);
        l2_123.setText(fiveTable.fiveScenario_ru[123]);
        l2_124.setText(fiveTable.fiveScenario_ru[124]);
        l2_125.setText(fiveTable.fiveScenario_ru[125]);
        l2_126.setText(fiveTable.fiveScenario_ru[126]);
        l2_127.setText(fiveTable.fiveScenario_ru[127]);
        l2_128.setText(fiveTable.fiveScenario_ru[128]);
        l2_129.setText(fiveTable.fiveScenario_ru[129]);





        l2_1.setVisibility(View.INVISIBLE);
        l2_2.setVisibility(View.INVISIBLE);
        l2_3.setVisibility(View.INVISIBLE);
        l2_4.setVisibility(View.INVISIBLE);
        l2_5.setVisibility(View.INVISIBLE);
        b1l2.setVisibility(View.INVISIBLE);
        b2l2.setVisibility(View.INVISIBLE);

        l2_6.setVisibility(View.INVISIBLE);
        l2_7.setVisibility(View.INVISIBLE);
        l2_8.setVisibility(View.INVISIBLE);
        l2_9.setVisibility(View.INVISIBLE);
        l2_10.setVisibility(View.INVISIBLE);
        l2_11.setVisibility(View.INVISIBLE);
        l2_12.setVisibility(View.INVISIBLE);
        l2_13.setVisibility(View.INVISIBLE);
        l2_14.setVisibility(View.INVISIBLE);
        l2_15.setVisibility(View.INVISIBLE);
        l2_16.setVisibility(View.INVISIBLE);
        l2_17.setVisibility(View.INVISIBLE);
        l2_18.setVisibility(View.INVISIBLE);
        l2_19.setVisibility(View.INVISIBLE);
        l2_20.setVisibility(View.INVISIBLE);
        l2_21.setVisibility(View.INVISIBLE);
        l2_22.setVisibility(View.INVISIBLE);
        l2_23.setVisibility(View.INVISIBLE);
        l2_24.setVisibility(View.INVISIBLE);

        l2_25.setVisibility(View.INVISIBLE);
        l2_26.setVisibility(View.INVISIBLE);
        l2_27.setVisibility(View.INVISIBLE);
        l2_28.setVisibility(View.INVISIBLE);
        l2_29.setVisibility(View.INVISIBLE);
        l2_30.setVisibility(View.INVISIBLE);
        l2_31.setVisibility(View.INVISIBLE);
        l2_32.setVisibility(View.INVISIBLE);
        l2_33.setVisibility(View.INVISIBLE);
        l2_34.setVisibility(View.INVISIBLE);
        l2_35.setVisibility(View.INVISIBLE);
        l2_36.setVisibility(View.INVISIBLE);
        l2_37.setVisibility(View.INVISIBLE);
        l2_38.setVisibility(View.INVISIBLE);
        l2_39.setVisibility(View.INVISIBLE);
        l2_40.setVisibility(View.INVISIBLE);
        l2_41.setVisibility(View.INVISIBLE);
        l2_42.setVisibility(View.INVISIBLE);
        l2_43.setVisibility(View.INVISIBLE);

        b3l2.setVisibility(View.INVISIBLE);
        b4l2.setVisibility(View.INVISIBLE);
        l2_44.setVisibility(View.INVISIBLE);

        l2_45.setVisibility(View.INVISIBLE);
        l2_46.setVisibility(View.INVISIBLE);
        l2_47.setVisibility(View.INVISIBLE);
        l2_48.setVisibility(View.INVISIBLE);
        l2_48_v.setVisibility(View.INVISIBLE);
        l2_49.setVisibility(View.INVISIBLE);
        l2_50.setVisibility(View.INVISIBLE);
        l2_51.setVisibility(View.INVISIBLE);
        l2_52.setVisibility(View.INVISIBLE);
        l2_53.setVisibility(View.INVISIBLE);
        l2_54.setVisibility(View.INVISIBLE);
        l2_55.setVisibility(View.INVISIBLE);
        l2_56.setVisibility(View.INVISIBLE);
        l2_57.setVisibility(View.INVISIBLE);
        l2_58.setVisibility(View.INVISIBLE);
        l2_59.setVisibility(View.INVISIBLE);
        l2_60.setVisibility(View.INVISIBLE);
        l2_61.setVisibility(View.INVISIBLE);
        l2_62.setVisibility(View.INVISIBLE);
        l2_63.setVisibility(View.INVISIBLE);
        l2_64.setVisibility(View.INVISIBLE);
        l2_65.setVisibility(View.INVISIBLE);
        l2_66.setVisibility(View.INVISIBLE);
        l2_67.setVisibility(View.INVISIBLE);
        l2_68.setVisibility(View.INVISIBLE);
        l2_69.setVisibility(View.INVISIBLE);
        l2_70.setVisibility(View.INVISIBLE);
        l2_71.setVisibility(View.INVISIBLE);
        l2_72.setVisibility(View.INVISIBLE);
        l2_73.setVisibility(View.INVISIBLE);
        l2_74.setVisibility(View.INVISIBLE);
        l2_75.setVisibility(View.INVISIBLE);
        l2_76.setVisibility(View.INVISIBLE);
        l2_77.setVisibility(View.INVISIBLE);
        l2_78.setVisibility(View.INVISIBLE);
        l2_79.setVisibility(View.INVISIBLE);
        l2_80.setVisibility(View.INVISIBLE);
        l2_81.setVisibility(View.INVISIBLE);
        l2_82.setVisibility(View.INVISIBLE);
        l2_83.setVisibility(View.INVISIBLE);
        l2_84.setVisibility(View.INVISIBLE);
        l2_85.setVisibility(View.INVISIBLE);
        l2_86.setVisibility(View.INVISIBLE);
        l2_87.setVisibility(View.INVISIBLE);
        l2_88.setVisibility(View.INVISIBLE);
        l2_89.setVisibility(View.INVISIBLE);
        l2_90.setVisibility(View.INVISIBLE);
        l2_91.setVisibility(View.INVISIBLE);
        l2_92.setVisibility(View.INVISIBLE);
        l2_93.setVisibility(View.INVISIBLE);
        l2_94.setVisibility(View.INVISIBLE);
        l2_95.setVisibility(View.INVISIBLE);
        l2_96.setVisibility(View.INVISIBLE);
        l2_97.setVisibility(View.INVISIBLE);
        l2_98.setVisibility(View.INVISIBLE);
        l2_99.setVisibility(View.INVISIBLE);
        l2_100.setVisibility(View.INVISIBLE);
        l2_101.setVisibility(View.INVISIBLE);
        l2_102.setVisibility(View.INVISIBLE);
        l2_103.setVisibility(View.INVISIBLE);
        l2_104.setVisibility(View.INVISIBLE);
        l2_105.setVisibility(View.INVISIBLE);
        l2_106.setVisibility(View.INVISIBLE);
        l2_107.setVisibility(View.INVISIBLE);
        l2_108.setVisibility(View.INVISIBLE);
        l2_109.setVisibility(View.INVISIBLE);
        l2_110.setVisibility(View.INVISIBLE);
        l2_111.setVisibility(View.INVISIBLE);
        l2_112.setVisibility(View.INVISIBLE);
        l2_113.setVisibility(View.INVISIBLE);
        l2_114.setVisibility(View.INVISIBLE);
        l2_115.setVisibility(View.INVISIBLE);
        l2_116.setVisibility(View.INVISIBLE);
        l2_117.setVisibility(View.INVISIBLE);
        l2_118.setVisibility(View.INVISIBLE);
        l2_119.setVisibility(View.INVISIBLE);
        l2_120.setVisibility(View.INVISIBLE);
        l2_121.setVisibility(View.INVISIBLE);
        l2_122.setVisibility(View.INVISIBLE);
        l2_123.setVisibility(View.INVISIBLE);
        l2_124.setVisibility(View.INVISIBLE);
        l2_125.setVisibility(View.INVISIBLE);
        l2_126.setVisibility(View.INVISIBLE);
        l2_127.setVisibility(View.INVISIBLE);
        l2_128.setVisibility(View.INVISIBLE);
        l2_129.setVisibility(View.INVISIBLE);

        b5l2.setVisibility(View.INVISIBLE);


        delay.execute();




    }
    class Delay extends AsyncTask<Void, Integer, Void> {


        @Override
        protected Void doInBackground(Void... voids) {
            while (line<=130){
                if(counterFirstLine == 0 && line==49){
                    line=49;
                    if (isCancelled())return null;
                }
                else
                if (counterSecondLine==0 && line==67){
                    line=67;
                    if (isCancelled())return null;
                }
                else
                if (counterSecondLine==2 && line==67){
                    line=68;
                    if (isCancelled())return null;
                }

                else {


                    publishProgress(line++);}

                try {
                    Thread.sleep(2500);
                    if (isCancelled())return null;
                }
                catch (Exception e){
                    e.printStackTrace( );
                }

            }


            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {


            final TextView l2_1 = findViewById(R.id.l5_1);
            final     TextView l2_2 = findViewById(R.id.l5_2);
            final    TextView l2_3 = findViewById(R.id.l5_3);
            final    TextView  l2_4 = findViewById(R.id.l5_4);
            final    TextView l2_5  = findViewById(R.id.l5_5);

            final  TextView l2_6  = findViewById(R.id.l5_6);
            final  TextView l2_7  = findViewById(R.id.l5_7);
            final  TextView l2_8  = findViewById(R.id.l5_8);
            final  TextView l2_9  = findViewById(R.id.l5_9);
            final TextView  l2_10 = findViewById(R.id.l5_10);
            final  TextView l2_11  = findViewById(R.id.l5_11);
            final  TextView l2_12  = findViewById(R.id.l5_12);
            final  TextView l2_13  = findViewById(R.id.l5_13);
            final  TextView l2_14  = findViewById(R.id.l5_14);
            final  TextView l2_15  = findViewById(R.id.l5_15);
            final  TextView l2_16  = findViewById(R.id.l5_16);
            final TextView l2_17  = findViewById(R.id.l5_17);
            final TextView l2_18  = findViewById(R.id.l5_18);
            final TextView l2_19  = findViewById(R.id.l5_19);
            final TextView l2_20  = findViewById(R.id.l5_20);
            final TextView  l2_21 = findViewById(R.id.l5_21);
            final  TextView l2_22  = findViewById(R.id.l5_22);
            final   TextView l2_23  = findViewById(R.id.l5_23);
            final  TextView l2_24  = findViewById(R.id.l5_24);

            final TextView l2_25  = findViewById(R.id.l5_25);
            final  TextView l2_26  = findViewById(R.id.l5_26);
            final  TextView l2_27  = findViewById(R.id.l5_27);
            final  TextView l2_28  = findViewById(R.id.l5_28);
            final  TextView l2_29  = findViewById(R.id.l5_29);
            final  TextView l2_30  = findViewById(R.id.l5_30);
            final  TextView l2_31  = findViewById(R.id.l5_31);
            final  TextView l2_32  = findViewById(R.id.l5_32);
            final TextView l2_33  = findViewById(R.id.l5_33);
            final TextView l2_34  = findViewById(R.id.l5_34);
            final TextView l2_35  = findViewById(R.id.l5_35);
            final  TextView l2_36  = findViewById(R.id.l5_36);
            final TextView l2_37  = findViewById(R.id.l5_37);
            final TextView l2_38  = findViewById(R.id.l5_38);
            final TextView l2_39  = findViewById(R.id.l5_39);
            final TextView l2_40  = findViewById(R.id.l5_40);
            final TextView l2_41  = findViewById(R.id.l5_41);
            final TextView l2_42  = findViewById(R.id.l5_42);
            final  TextView l2_43  = findViewById(R.id.l5_43);

            final  TextView l2_44  = findViewById(R.id.l5_44);

            final   TextView l2_45  = findViewById(R.id.l5_45);
            final   TextView l2_46  = findViewById(R.id.l5_46);
            final  TextView l2_47  = findViewById(R.id.l5_47);
            final  TextView l2_48  = findViewById(R.id.l5_48);
            final  TextView l2_48_v  = findViewById(R.id.l5_48_v);



            final   Button b1l2  = findViewById(R.id.b1l5);
            final  Button b2l2  = findViewById(R.id.b2l5);




            final  TextView l2_49  = findViewById(R.id.l5_49);
            final  TextView l2_50  = findViewById(R.id.l5_50);
            final  TextView l2_51  = findViewById(R.id.l5_51);
            final  TextView l2_52  = findViewById(R.id.l5_52);
            final   TextView l2_53  = findViewById(R.id.l5_53);
            final   TextView l2_54  = findViewById(R.id.l5_54);
            final  TextView l2_55 = findViewById(R.id.l5_55);
            final  TextView l2_56 = findViewById(R.id.l5_56);
            final TextView l2_57 = findViewById(R.id.l5_57);
            final TextView l2_58 = findViewById(R.id.l5_58);
            final TextView l2_59  = findViewById(R.id.l5_59);
            final  TextView l2_60  = findViewById(R.id.l5_60);
            final  TextView l2_61  = findViewById(R.id.l5_61);
            final TextView l2_62  = findViewById(R.id.l5_62);
            final  TextView l2_63  = findViewById(R.id.l5_63);
            final  TextView l2_64  = findViewById(R.id.l5_64);
            final  TextView l2_65  = findViewById(R.id.l5_65);
            final  TextView l2_66  = findViewById(R.id.l5_66);
            final  TextView l2_67  = findViewById(R.id.l5_67);





            final  Button b3l2  = findViewById(R.id.b3l5);
            final  Button b4l2  = findViewById(R.id.b4l5);


            final TextView l2_68= findViewById(R.id.l5_68);


            TextView l2_69= findViewById(R.id.l5_69);
            TextView l2_70= findViewById(R.id.l5_70);
            TextView l2_71= findViewById(R.id.l5_71);
            TextView l2_72= findViewById(R.id.l5_72);
            TextView l2_73= findViewById(R.id.l5_73);
            TextView l2_74= findViewById(R.id.l5_74);
            TextView l2_75= findViewById(R.id.l5_75);
            TextView l2_76= findViewById(R.id.l5_76);
            TextView l2_77= findViewById(R.id.l5_77);
            TextView l2_78= findViewById(R.id.l5_78);
            TextView l2_79= findViewById(R.id.l5_79);
            TextView l2_80= findViewById(R.id.l5_80);
            TextView l2_81= findViewById(R.id.l5_81);
            TextView l2_82= findViewById(R.id.l5_82);
            TextView l2_83= findViewById(R.id.l5_83);
            TextView l2_84= findViewById(R.id.l5_84);
            TextView l2_85= findViewById(R.id.l5_85);
            TextView l2_86= findViewById(R.id.l5_86);
            TextView l2_87= findViewById(R.id.l5_87);
            TextView l2_88= findViewById(R.id.l5_88);
            TextView l2_89= findViewById(R.id.l5_89);
            TextView l2_90= findViewById(R.id.l5_90);
            TextView l2_91= findViewById(R.id.l5_91);
            TextView l2_92= findViewById(R.id.l5_92);
            TextView l2_93= findViewById(R.id.l5_93);
            TextView l2_94= findViewById(R.id.l5_94);
            TextView l2_95= findViewById(R.id.l5_95);
            TextView l2_96= findViewById(R.id.l5_96);
            TextView l2_97= findViewById(R.id.l5_97);
            TextView l2_98= findViewById(R.id.l5_98);
            TextView l2_99= findViewById(R.id.l5_99);
            TextView l2_100= findViewById(R.id.l5_100);
            TextView l2_101= findViewById(R.id.l5_101);
            TextView l2_102= findViewById(R.id.l5_102);
            TextView l2_103= findViewById(R.id.l5_103);
            TextView l2_104= findViewById(R.id.l5_104);
            TextView l2_105= findViewById(R.id.l5_105);
            TextView l2_106= findViewById(R.id.l5_106);
            TextView l2_107= findViewById(R.id.l5_107);
            TextView l2_108= findViewById(R.id.l5_108);
            TextView l2_109= findViewById(R.id.l5_109);
            TextView l2_110= findViewById(R.id.l5_110);
            TextView l2_111= findViewById(R.id.l5_111);
            TextView l2_112= findViewById(R.id.l5_112);
            TextView l2_113= findViewById(R.id.l5_113);
            TextView l2_114= findViewById(R.id.l5_114);
            TextView l2_115= findViewById(R.id.l5_115);
            TextView l2_116= findViewById(R.id.l5_116);
            TextView l2_117= findViewById(R.id.l5_117);
            TextView l2_118= findViewById(R.id.l5_118);
            TextView l2_119= findViewById(R.id.l5_119);
            TextView l2_120= findViewById(R.id.l5_120);
            TextView l2_121= findViewById(R.id.l5_121);
            TextView l2_122= findViewById(R.id.l5_122);
            TextView l2_123= findViewById(R.id.l5_123);
            TextView l2_124= findViewById(R.id.l5_124);
            TextView l2_125= findViewById(R.id.l5_125);
            TextView l2_126= findViewById(R.id.l5_126);
            TextView l2_127= findViewById(R.id.l5_127);
            TextView l2_128= findViewById(R.id.l5_128);
            TextView l2_129= findViewById(R.id.l5_129);
            final  Button    b5l2  = findViewById(R.id.b5l5);


            final Animation a = AnimationUtils.loadAnimation(Level5.this, R.anim.text_in_game);

            switch (line){
                case 0: l2_1.setVisibility(View.VISIBLE); l2_1.startAnimation(a); break;
                case 1: l2_2.setVisibility(View.VISIBLE); l2_2.startAnimation(a); break;
                case 2: l2_3.setVisibility(View.VISIBLE); l2_3.startAnimation(a); break;
                case 3: l2_4.setVisibility(View.VISIBLE); l2_4.startAnimation(a); break;
                case 4: l2_5.setVisibility(View.VISIBLE); l2_5.startAnimation(a); break;
                case 5:
                    break;

                case 6: l2_6.setVisibility(View.VISIBLE); l2_6.startAnimation(a); break;
                case 7: l2_7.setVisibility(View.VISIBLE); l2_7.startAnimation(a); break;
                case 8: l2_8.setVisibility(View.VISIBLE); l2_8.startAnimation(a); break;
                case 9: l2_9.setVisibility(View.VISIBLE); l2_9.startAnimation(a); break;
                case 10: l2_10.setVisibility(View.VISIBLE); l2_10.startAnimation(a); break;
                case 11: l2_11.setVisibility(View.VISIBLE); l2_11.startAnimation(a); break;
                case 12: l2_12.setVisibility(View.VISIBLE); l2_12.startAnimation(a); break;
                case 13: l2_13.setVisibility(View.VISIBLE); l2_13.startAnimation(a); break;
                case 14: l2_14.setVisibility(View.VISIBLE); l2_14.startAnimation(a); break;
                case 15: l2_15.setVisibility(View.VISIBLE); l2_15.startAnimation(a); break;
                case 16: l2_16.setVisibility(View.VISIBLE); l2_16.startAnimation(a); break;
                case 17: l2_17.setVisibility(View.VISIBLE); l2_17.startAnimation(a); break;
                case 18: l2_18.setVisibility(View.VISIBLE); l2_18.startAnimation(a); break;
                case 19: l2_19.setVisibility(View.VISIBLE); l2_19.startAnimation(a); break;
                case 20: l2_20.setVisibility(View.VISIBLE); l2_20.startAnimation(a); break;
                case 21: l2_21.setVisibility(View.VISIBLE); l2_21.startAnimation(a); break;
                case 22: l2_22.setVisibility(View.VISIBLE); l2_22.startAnimation(a); break;
                case 23: l2_23.setVisibility(View.VISIBLE); l2_23.startAnimation(a); break;
                case 24: l2_24.setVisibility(View.VISIBLE); l2_24.startAnimation(a);
                    break;

                case 25: l2_25.setVisibility(View.VISIBLE); l2_25.startAnimation(a); break;
                case 26: l2_26.setVisibility(View.VISIBLE); l2_26.startAnimation(a); break;
                case 27: l2_27.setVisibility(View.VISIBLE); l2_27.startAnimation(a); break;
                case 28: l2_28.setVisibility(View.VISIBLE); l2_28.startAnimation(a); break;
                case 29: l2_29.setVisibility(View.VISIBLE); l2_29.startAnimation(a); break;
                case 30: l2_30.setVisibility(View.VISIBLE); l2_30.startAnimation(a); break;
                case 31: l2_31.setVisibility(View.VISIBLE); l2_31.startAnimation(a); break;
                case 32: l2_32.setVisibility(View.VISIBLE); l2_32.startAnimation(a); break;
                case 33: l2_33.setVisibility(View.VISIBLE); l2_33.startAnimation(a); break;
                case 34: l2_34.setVisibility(View.VISIBLE); l2_34.startAnimation(a); break;
                case 35:l2_35.setVisibility(View.VISIBLE); l2_35.startAnimation(a);



                    break;

















                case 36:

                    l2_36.setVisibility(View.VISIBLE); l2_36.startAnimation(a); break;


                case 37: l2_37.setVisibility(View.VISIBLE); l2_37.startAnimation(a); break;
                case 38: l2_38.setVisibility(View.VISIBLE); l2_38.startAnimation(a); break;
                case 39: l2_39.setVisibility(View.VISIBLE); l2_39.startAnimation(a); break;
                case 40: l2_40.setVisibility(View.VISIBLE); l2_40.startAnimation(a); break;
                case 41: l2_41.setVisibility(View.VISIBLE); l2_41.startAnimation(a); break;
                case 42: l2_42.setVisibility(View.VISIBLE); l2_42.startAnimation(a); break;
                case 43: l2_43.setVisibility(View.VISIBLE); l2_43.startAnimation(a); break;
                case 44: l2_44.setVisibility(View.VISIBLE); l2_44.startAnimation(a); break;
                case 45:

                    break;
                case 46: l2_45.setVisibility(View.VISIBLE); l2_45.startAnimation(a); break;
                case 47: l2_46.setVisibility(View.VISIBLE); l2_46.startAnimation(a); break;
                case 48: l2_47.setVisibility(View.VISIBLE); l2_47.startAnimation(a); break;
                case 49: b1l2.setVisibility(View.VISIBLE); b1l2.startAnimation(a);
                    b1l2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {
                                if (counterFirstLine==0){

                                    b1l2.setBackgroundResource(R.drawable.btn_vibor_l2);



                                    counterFirstLine=1;
                                } }
                            catch (Exception e){

                            }
                        }
                    });
                    b2l2.setVisibility(View.VISIBLE); b2l2.startAnimation(a);
                    b2l2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if (counterFirstLine==0){
                                    b2l2.setBackgroundResource(R.drawable.btn_vibor_l2);

//                                    l2_6.setVisibility(View.GONE);
//                                    l2_7.setVisibility(View.GONE);
//                                    l2_8.setVisibility(View.GONE);
//                                    l2_9.setVisibility(View.GONE);
//                                    l2_10.setVisibility(View.GONE);
//                                    l2_11.setVisibility(View.GONE);
//                                    l2_12.setVisibility(View.GONE);
//                                    l2_13.setVisibility(View.GONE);
                                    l2_48_v.setVisibility(View.GONE);
                                    l2_48.setVisibility(View.GONE);

                                    line=51;
                                    counterFirstLine=1;
                                }}
                            catch (Exception e){

                            }
                        }
                    });












                     break;
                case 50:  l2_48_v.setVisibility(View.VISIBLE); l2_48_v.startAnimation(a); l2_48.setVisibility(View.VISIBLE); l2_48.startAnimation(a);  break;
                case 51: l2_49.setVisibility(View.VISIBLE); l2_49.startAnimation(a); l2_50.setVisibility(View.VISIBLE); l2_50.startAnimation(a); break;
                case 52: l2_51.setVisibility(View.VISIBLE); l2_51.startAnimation(a); break;
                case 53: l2_52.setVisibility(View.VISIBLE); l2_52.startAnimation(a); break;
                case 54: l2_53.setVisibility(View.VISIBLE); l2_53.startAnimation(a); break;
                case 55: l2_54.setVisibility(View.VISIBLE); l2_54.startAnimation(a); break;
                case 56: l2_55.setVisibility(View.VISIBLE); l2_55.startAnimation(a); break;
                case 57: l2_56.setVisibility(View.VISIBLE); l2_56.startAnimation(a); break;
                case 58: l2_57.setVisibility(View.VISIBLE); l2_57.startAnimation(a); break;
                case 59: l2_58.setVisibility(View.VISIBLE); l2_58.startAnimation(a); break;
                case 60: l2_59.setVisibility(View.VISIBLE); l2_59.startAnimation(a); break;
                case 61: l2_60.setVisibility(View.VISIBLE); l2_60.startAnimation(a); break;
                case 62: l2_61.setVisibility(View.VISIBLE); l2_61.startAnimation(a); break;
                case 63: l2_62.setVisibility(View.VISIBLE); l2_62.startAnimation(a); break;
                case 64: l2_63.setVisibility(View.VISIBLE); l2_63.startAnimation(a); break;
                case 65: l2_64.setVisibility(View.VISIBLE); l2_64.startAnimation(a); break;
                case 66: l2_65.setVisibility(View.VISIBLE); l2_65.startAnimation(a); l2_66.setVisibility(View.VISIBLE); l2_66.startAnimation(a); break;
                case 67: l2_67.setVisibility(View.VISIBLE); l2_67.startAnimation(a);



                    b3l2.setVisibility(View.VISIBLE); b3l2.startAnimation(a);
                    b3l2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if(counterSecondLine==0){
                                    b3l2.setBackgroundResource(R.drawable.btn_vibor_l2);
                                    l2_68.setVisibility(View.GONE);
                                    line=67;
                                    counterSecondLine=2;
                                }

                            }
                            catch (Exception e){

                            }
                        }
                    });


                    b4l2.setVisibility(View.VISIBLE); b4l2.startAnimation(a);
                    b4l2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if(counterSecondLine==0){
                                    b4l2.setBackgroundResource(R.drawable.btn_vibor_l2);



                                    line=67;
                                    counterSecondLine=1;



                                }}
                            catch (Exception e){

                            }
                        }
                    });



                    break;


                case 68: l2_68.setVisibility(View.VISIBLE); l2_68.startAnimation(a); break;
                case 69: l2_69.setVisibility(View.VISIBLE); l2_69.startAnimation(a); break;
                case 70: l2_70.setVisibility(View.VISIBLE); l2_70.startAnimation(a); break;
                case 71: l2_71.setVisibility(View.VISIBLE); l2_71.startAnimation(a); break;
                case 72: l2_72.setVisibility(View.VISIBLE); l2_72.startAnimation(a); break;
                case 73: l2_73.setVisibility(View.VISIBLE); l2_73.startAnimation(a); break;
                case 74: l2_74.setVisibility(View.VISIBLE); l2_74.startAnimation(a); break;
                case 75: l2_75.setVisibility(View.VISIBLE); l2_75.startAnimation(a); break;
                case 76: l2_76.setVisibility(View.VISIBLE); l2_76.startAnimation(a); break;
                case 77: l2_77.setVisibility(View.VISIBLE); l2_77.startAnimation(a); break;
                case 78: l2_78.setVisibility(View.VISIBLE); l2_78.startAnimation(a); break;
                case 79: l2_79.setVisibility(View.VISIBLE); l2_79.startAnimation(a); break;
                case 80: l2_80.setVisibility(View.VISIBLE); l2_80.startAnimation(a); break;
                case 81: l2_81.setVisibility(View.VISIBLE); l2_81.startAnimation(a); break;
                case 82: l2_82.setVisibility(View.VISIBLE); l2_82.startAnimation(a); break;
                case 83: l2_83.setVisibility(View.VISIBLE); l2_83.startAnimation(a); break;
                case 84: l2_84.setVisibility(View.VISIBLE); l2_84.startAnimation(a); break;
                case 85: l2_85.setVisibility(View.VISIBLE); l2_85.startAnimation(a); break;
                case 86: l2_86.setVisibility(View.VISIBLE); l2_86.startAnimation(a); break;
                case 87: l2_87.setVisibility(View.VISIBLE); l2_87.startAnimation(a); break;
                case 88: l2_88.setVisibility(View.VISIBLE); l2_88.startAnimation(a); break;
                case 89: l2_89.setVisibility(View.VISIBLE); l2_89.startAnimation(a); break;
                case 90: l2_90.setVisibility(View.VISIBLE); l2_90.startAnimation(a); break;
                case 91: l2_91.setVisibility(View.VISIBLE); l2_91.startAnimation(a); break;
                case 92: l2_92.setVisibility(View.VISIBLE); l2_92.startAnimation(a); break;
                case 93: l2_93.setVisibility(View.VISIBLE); l2_93.startAnimation(a); break;
                case 94: l2_94.setVisibility(View.VISIBLE); l2_94.startAnimation(a); break;
                case 95: l2_95.setVisibility(View.VISIBLE); l2_95.startAnimation(a); break;
                case 96: l2_96.setVisibility(View.VISIBLE); l2_96.startAnimation(a); break;
                case 97: l2_97.setVisibility(View.VISIBLE); l2_97.startAnimation(a); break;
                case 98: l2_98.setVisibility(View.VISIBLE); l2_98.startAnimation(a); break;
                case 99: l2_99.setVisibility(View.VISIBLE); l2_99.startAnimation(a); break;
                case 100: l2_100.setVisibility(View.VISIBLE); l2_100.startAnimation(a); break;
                case 101: l2_101.setVisibility(View.VISIBLE); l2_101.startAnimation(a); break;
                case 102: l2_102.setVisibility(View.VISIBLE); l2_102.startAnimation(a); break;
                case 103: l2_103.setVisibility(View.VISIBLE); l2_103.startAnimation(a); break;
                case 104: l2_104.setVisibility(View.VISIBLE); l2_104.startAnimation(a); break;
                case 105: l2_105.setVisibility(View.VISIBLE); l2_105.startAnimation(a); break;
                case 106: l2_106.setVisibility(View.VISIBLE); l2_106.startAnimation(a); break;
                case 107: l2_107.setVisibility(View.VISIBLE); l2_107.startAnimation(a); break;
                case 108: l2_108.setVisibility(View.VISIBLE); l2_108.startAnimation(a); break;
                case 109: l2_109.setVisibility(View.VISIBLE); l2_109.startAnimation(a); break;
                case 110: l2_110.setVisibility(View.VISIBLE); l2_110.startAnimation(a); break;
                case 111: l2_111.setVisibility(View.VISIBLE); l2_111.startAnimation(a); break;
                case 112: l2_112.setVisibility(View.VISIBLE); l2_112.startAnimation(a); break;
                case 113: l2_113.setVisibility(View.VISIBLE); l2_113.startAnimation(a); break;
                case 114: l2_114.setVisibility(View.VISIBLE); l2_114.startAnimation(a); break;
                case 115: l2_115.setVisibility(View.VISIBLE); l2_115.startAnimation(a); break;
                case 116: l2_116.setVisibility(View.VISIBLE); l2_116.startAnimation(a); break;
                case 117: l2_117.setVisibility(View.VISIBLE); l2_117.startAnimation(a); break;
                case 118: l2_118.setVisibility(View.VISIBLE); l2_118.startAnimation(a); break;
                case 119: l2_119.setVisibility(View.VISIBLE); l2_119.startAnimation(a); break;
                case 120: l2_120.setVisibility(View.VISIBLE); l2_120.startAnimation(a); break;
                case 121: l2_121.setVisibility(View.VISIBLE); l2_121.startAnimation(a); break;
                case 122: l2_122.setVisibility(View.VISIBLE); l2_122.startAnimation(a); break;
                case 123: l2_123.setVisibility(View.VISIBLE); l2_123.startAnimation(a); break;
                case 124: l2_124.setVisibility(View.VISIBLE); l2_124.startAnimation(a); break;
                case 125: l2_125.setVisibility(View.VISIBLE); l2_125.startAnimation(a); break;
                case 126: l2_126.setVisibility(View.VISIBLE); l2_126.startAnimation(a); break;
                case 127: l2_127.setVisibility(View.VISIBLE); l2_127.startAnimation(a); break;
                case 128: l2_128.setVisibility(View.VISIBLE); l2_128.startAnimation(a); break;
                case 129: l2_129.setVisibility(View.VISIBLE); l2_129.startAnimation(a); break;
                case 130:
                                        b5l2.setVisibility(View.VISIBLE); b5l2.startAnimation(a);
                    b5l2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                b5l2.setBackgroundResource(R.drawable.btn_vibor_l2);
                                Intent lv4 = new Intent(Level5.this, Preview_lvl5.class);
                                startActivity(lv4);
                                finish();

                            }
                            catch (Exception e){

                            }
                        }
                    }); break;









                default:break;




            }

        }
    }
    @Override

    public void onBackPressed() {

        super.onBackPressed();

    }

}




