package com.ggs.harryandrationalthinking.levels.Level3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ggs.harryandrationalthinking.R;
import com.ggs.harryandrationalthinking.data.ThreeTable;
import com.ggs.harryandrationalthinking.levels.Level4.Preview_Lvl4;
import com.ggs.harryandrationalthinking.levels.level2.Level2;

import java.util.logging.Level;


public class Level3 extends AppCompatActivity {
    ThreeTable threeTable = new ThreeTable();
    TextView l2_1;
    TextView l2_2;
    TextView l2_3;
    TextView l2_4;
    TextView l2_5;
    Button b1l2;
    Button b2l2;
    TextView l2_6;
    TextView l2_7;
    TextView l2_8;
    TextView l2_9;
    TextView l2_10;
    TextView l2_11;
    TextView l2_12;
    TextView l2_13;
    TextView l2_14;
    TextView l2_15;
    TextView l2_16;
    TextView l2_17;
    TextView l2_18;
    TextView l2_19;
    TextView l2_20;
    TextView l2_21;
    TextView l2_22;
    TextView l2_23;
    TextView l2_24;

    TextView l2_25;
    TextView l2_26;
    TextView l2_27;
    TextView l2_28;
    TextView l2_29;
    TextView l2_30;
    TextView l2_31;
    TextView l2_32;
    TextView l2_33;
    TextView l2_34;
    TextView l2_35;
    TextView l2_36;
    TextView l2_37;
    TextView l2_38;
    TextView l2_39;
    TextView l2_40;
    TextView l2_41;
    TextView l2_42;
    TextView l2_43;
    Button b3l2;
    Button b4l2;
    TextView l2_44;
     TextView l2_45;
    TextView l2_46;
    TextView l2_47;
    TextView l2_48;
    TextView l2_49;
    TextView l2_50;
    TextView l2_51;
    TextView l2_52;
    TextView l2_53;
    TextView l2_54;
    TextView l2_55;
    TextView l2_56;
    TextView l2_57;
    TextView l2_58;
    TextView l2_59;
    TextView l2_60;
    TextView l2_61;
    TextView l2_62;
    TextView l2_63;
    TextView l2_64;
    TextView l2_65;
    TextView l2_66;
    Button    b5l2;
    public Animation a;
    public int line = -1;
    int counterFirstLine = 0;
    int counterSecondLine = 0;
    Delay delay = new Delay();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level3);
        SharedPreferences save = getSharedPreferences("Save", MODE_PRIVATE);
        SharedPreferences.Editor editor = save.edit();
        editor.putInt("Level", 3);
        editor.commit();

        final TextView l2_1 = findViewById(R.id.l3_1);
        final     TextView l2_2 = findViewById(R.id.l3_2);
        final    TextView l2_3 = findViewById(R.id.l3_3);
        final    TextView  l2_4 = findViewById(R.id.l3_4);
        final    TextView l2_5  = findViewById(R.id.l3_5);
        final   Button b1l2  = findViewById(R.id.b1l3);
        final  Button b2l2  = findViewById(R.id.b2l3);
        final  TextView l2_6  = findViewById(R.id.l3_6);
        final  TextView l2_7  = findViewById(R.id.l3_7);
        final  TextView l2_8  = findViewById(R.id.l3_8);
        final  TextView l2_9  = findViewById(R.id.l3_9);
        final TextView  l2_10 = findViewById(R.id.l3_10);
        final  TextView l2_11  = findViewById(R.id.l3_11);
        final  TextView l2_12  = findViewById(R.id.l3_12);
        final  TextView l2_13  = findViewById(R.id.l3_13);
        final  TextView l2_14  = findViewById(R.id.l3_14);
        final  TextView l2_15  = findViewById(R.id.l3_15);
        final  TextView l2_16  = findViewById(R.id.l3_16);
        final TextView l2_17  = findViewById(R.id.l3_17);
        final TextView l2_18  = findViewById(R.id.l3_18);
        final TextView l2_19  = findViewById(R.id.l3_19);
        final TextView l2_20  = findViewById(R.id.l3_20);
        final TextView  l2_21 = findViewById(R.id.l3_21);
        final  TextView l2_22  = findViewById(R.id.l3_22);
        final   TextView l2_23  = findViewById(R.id.l3_23);
        final  TextView l2_24  = findViewById(R.id.l3_24);

        final TextView l2_25  = findViewById(R.id.l3_25);
        final  TextView l2_26  = findViewById(R.id.l3_26);
        final  TextView l2_27  = findViewById(R.id.l3_27);
        final  TextView l2_28  = findViewById(R.id.l3_28);
        final  TextView l2_29  = findViewById(R.id.l3_29);
        final  TextView l2_30  = findViewById(R.id.l3_30);
        final  TextView l2_31  = findViewById(R.id.l3_31);
        final  TextView l2_32  = findViewById(R.id.l3_32);
        final TextView l2_33  = findViewById(R.id.l3_33);
        final TextView l2_34  = findViewById(R.id.l3_34);
        final TextView l2_35  = findViewById(R.id.l3_35);
        final  TextView l2_36  = findViewById(R.id.l3_36);
        final TextView l2_37  = findViewById(R.id.l3_37);
        final TextView l2_38  = findViewById(R.id.l3_38);
        final TextView l2_39  = findViewById(R.id.l3_39);
        final TextView l2_40  = findViewById(R.id.l3_40);
        final TextView l2_41  = findViewById(R.id.l3_41);
        final TextView l2_42  = findViewById(R.id.l3_42);
        final  TextView l2_43  = findViewById(R.id.l3_43);
        final  Button b3l2  = findViewById(R.id.b3l3);
        final  Button b4l2  = findViewById(R.id.b4l3);
        final  TextView l2_44  = findViewById(R.id.l3_44);

        final   TextView l2_45  = findViewById(R.id.l3_45);
        final   TextView l2_46  = findViewById(R.id.l3_46);
        final  TextView l2_47  = findViewById(R.id.l3_47);
        final  TextView l2_48  = findViewById(R.id.l3_48);
        final  TextView l2_49  = findViewById(R.id.l3_49);
        final  TextView l2_50  = findViewById(R.id.l3_50);
        final  TextView l2_51  = findViewById(R.id.l3_51);
        final  TextView l2_52  = findViewById(R.id.l3_52);
        final   TextView l2_53  = findViewById(R.id.l3_53);
        final   TextView l2_54  = findViewById(R.id.l3_54);
        final  TextView l2_55 = findViewById(R.id.l3_55);
        final  TextView l2_56 = findViewById(R.id.l3_56);
        final TextView l2_57 = findViewById(R.id.l3_57);
        final TextView l2_58 = findViewById(R.id.l3_58);
        final TextView l2_59  = findViewById(R.id.l3_59);
        final  TextView l2_60  = findViewById(R.id.l3_60);
        final  TextView l2_61  = findViewById(R.id.l3_61);
        final TextView l2_62  = findViewById(R.id.l3_62);
        final  TextView l2_63  = findViewById(R.id.l3_63);
        final  TextView l2_64  = findViewById(R.id.l3_64);
        final  TextView l2_65  = findViewById(R.id.l3_65);
        final  TextView l2_66  = findViewById(R.id.l3_66);
        final  Button    b5l2  = findViewById(R.id.b5l3);

        l2_1.setText(threeTable.threeScenario_ru[0]);
        l2_2.setText(threeTable.threeScenario_ru[1]);
        l2_3.setText(threeTable.threeScenario_ru[2]);
        l2_4.setText(threeTable.threeScenario_ru[3]);
        l2_5.setText(threeTable.threeScenario_ru[4]);
        l2_6.setText(threeTable.threeScenario_ru[5]);
        l2_7.setText(threeTable.threeScenario_ru[6]);
        l2_8.setText(threeTable.threeScenario_ru[7]);
        l2_9.setText(threeTable.threeScenario_ru[8]);
        l2_10.setText(threeTable.threeScenario_ru[9]);
        l2_11.setText(threeTable.threeScenario_ru[10]);
        l2_12.setText(threeTable.threeScenario_ru[11]);
        l2_13.setText(threeTable.threeScenario_ru[12]);
        l2_14.setText(threeTable.threeScenario_ru[13]);
        l2_15.setText(threeTable.threeScenario_ru[14]);
        l2_16.setText(threeTable.threeScenario_ru[15]);
        l2_17.setText(threeTable.threeScenario_ru[16]);
        l2_18.setText(threeTable.threeScenario_ru[17]);
        l2_19.setText(threeTable.threeScenario_ru[18]);
        l2_20.setText(threeTable.threeScenario_ru[19]);
        l2_21.setText(threeTable.threeScenario_ru[20]);
        l2_22.setText(threeTable.threeScenario_ru[21]);
        l2_23.setText(threeTable.threeScenario_ru[22]);
        l2_24.setText(threeTable.threeScenario_ru[23]);
        l2_25.setText(threeTable.threeScenario_ru[24]);
        l2_26.setText(threeTable.threeScenario_ru[25]);
        l2_27.setText(threeTable.threeScenario_ru[26]);
        l2_28.setText(threeTable.threeScenario_ru[27]);
        l2_29.setText(threeTable.threeScenario_ru[28]);
        l2_30.setText(threeTable.threeScenario_ru[29]);
        l2_31.setText(threeTable.threeScenario_ru[30]);
        l2_32.setText(threeTable.threeScenario_ru[31]);
        l2_33.setText(threeTable.threeScenario_ru[32]);
        l2_34.setText(threeTable.threeScenario_ru[33]);
        l2_35.setText(threeTable.threeScenario_ru[34]);
        l2_36.setText(threeTable.threeScenario_ru[35]);
        l2_37.setText(threeTable.threeScenario_ru[36]);
        l2_38.setText(threeTable.threeScenario_ru[37]);
        l2_39.setText(threeTable.threeScenario_ru[38]);
        l2_40.setText(threeTable.threeScenario_ru[39]);
        l2_41.setText(threeTable.threeScenario_ru[40]);
        l2_42.setText(threeTable.threeScenario_ru[41]);
        l2_43.setText(threeTable.threeScenario_ru[42]);
        l2_44.setText(threeTable.threeScenario_ru[43]);
        l2_45.setText(threeTable.threeScenario_ru[44]);
        l2_46.setText(threeTable.threeScenario_ru[45]);
        l2_47.setText(threeTable.threeScenario_ru[46]);
        l2_48.setText(threeTable.threeScenario_ru[47]);
        l2_49.setText(threeTable.threeScenario_ru[48]);
        l2_50.setText(threeTable.threeScenario_ru[49]);
        l2_51.setText(threeTable.threeScenario_ru[50]);
        l2_52.setText(threeTable.threeScenario_ru[51]);
        l2_53.setText(threeTable.threeScenario_ru[52]);
        l2_54.setText(threeTable.threeScenario_ru[53]);
        l2_55.setText(threeTable.threeScenario_ru[54]);
        l2_56.setText(threeTable.threeScenario_ru[55]);
        l2_57.setText(threeTable.threeScenario_ru[56]);
        l2_58.setText(threeTable.threeScenario_ru[57]);
        l2_59.setText(threeTable.threeScenario_ru[58]);
        l2_60.setText(threeTable.threeScenario_ru[59]);
        l2_61.setText(threeTable.threeScenario_ru[60]);
        l2_62.setText(threeTable.threeScenario_ru[61]);
        l2_63.setText(threeTable.threeScenario_ru[62]);
        l2_64.setText(threeTable.threeScenario_ru[63]);
        l2_65.setText(threeTable.threeScenario_ru[64]);
        l2_66.setText(threeTable.threeScenario_ru[65]);







        l2_1.setVisibility(View.INVISIBLE);
        l2_2.setVisibility(View.INVISIBLE);
        l2_3.setVisibility(View.INVISIBLE);
        l2_4.setVisibility(View.INVISIBLE);
        l2_5.setVisibility(View.INVISIBLE);
        b1l2.setVisibility(View.INVISIBLE);
        b2l2.setVisibility(View.INVISIBLE);

        l2_6.setVisibility(View.INVISIBLE);
        l2_7.setVisibility(View.INVISIBLE);
        l2_8.setVisibility(View.INVISIBLE);
        l2_9.setVisibility(View.INVISIBLE);
        l2_10.setVisibility(View.INVISIBLE);
        l2_11.setVisibility(View.INVISIBLE);
        l2_12.setVisibility(View.INVISIBLE);
        l2_13.setVisibility(View.INVISIBLE);
        l2_14.setVisibility(View.INVISIBLE);
        l2_15.setVisibility(View.INVISIBLE);
        l2_16.setVisibility(View.INVISIBLE);
        l2_17.setVisibility(View.INVISIBLE);
        l2_18.setVisibility(View.INVISIBLE);
        l2_19.setVisibility(View.INVISIBLE);
        l2_20.setVisibility(View.INVISIBLE);
        l2_21.setVisibility(View.INVISIBLE);
        l2_22.setVisibility(View.INVISIBLE);
        l2_23.setVisibility(View.INVISIBLE);
        l2_24.setVisibility(View.INVISIBLE);

        l2_25.setVisibility(View.INVISIBLE);
        l2_26.setVisibility(View.INVISIBLE);
        l2_27.setVisibility(View.INVISIBLE);
        l2_28.setVisibility(View.INVISIBLE);
        l2_29.setVisibility(View.INVISIBLE);
        l2_30.setVisibility(View.INVISIBLE);
        l2_31.setVisibility(View.INVISIBLE);
        l2_32.setVisibility(View.INVISIBLE);
        l2_33.setVisibility(View.INVISIBLE);
        l2_34.setVisibility(View.INVISIBLE);
        l2_35.setVisibility(View.INVISIBLE);
        l2_36.setVisibility(View.INVISIBLE);
        l2_37.setVisibility(View.INVISIBLE);
        l2_38.setVisibility(View.INVISIBLE);
        l2_39.setVisibility(View.INVISIBLE);
        l2_40.setVisibility(View.INVISIBLE);
        l2_41.setVisibility(View.INVISIBLE);
        l2_42.setVisibility(View.INVISIBLE);
        l2_43.setVisibility(View.INVISIBLE);

        b3l2.setVisibility(View.INVISIBLE);
        b4l2.setVisibility(View.INVISIBLE);
        l2_44.setVisibility(View.INVISIBLE);

        l2_45.setVisibility(View.INVISIBLE);
        l2_46.setVisibility(View.INVISIBLE);
        l2_47.setVisibility(View.INVISIBLE);
        l2_48.setVisibility(View.INVISIBLE);
        l2_49.setVisibility(View.INVISIBLE);
        l2_50.setVisibility(View.INVISIBLE);
        l2_51.setVisibility(View.INVISIBLE);
        l2_52.setVisibility(View.INVISIBLE);
        l2_53.setVisibility(View.INVISIBLE);
        l2_54.setVisibility(View.INVISIBLE);
        l2_55.setVisibility(View.INVISIBLE);
        l2_56.setVisibility(View.INVISIBLE);
        l2_57.setVisibility(View.INVISIBLE);
        l2_58.setVisibility(View.INVISIBLE);
        l2_59.setVisibility(View.INVISIBLE);
        l2_60.setVisibility(View.INVISIBLE);
        l2_61.setVisibility(View.INVISIBLE);
        l2_62.setVisibility(View.INVISIBLE);
        l2_63.setVisibility(View.INVISIBLE);
        l2_64.setVisibility(View.INVISIBLE);
        l2_65.setVisibility(View.INVISIBLE);
        l2_66.setVisibility(View.INVISIBLE);
        b5l2.setVisibility(View.INVISIBLE);

        delay.execute();




    }
    class Delay extends AsyncTask<Void, Integer, Void> {


        @Override
        protected Void doInBackground(Void... voids) {
            while (line<=67){
                if(counterFirstLine == 0 && line==5){
                    line=5;
                    if (isCancelled())return null;
                }
                else
                if (counterSecondLine==0 && line==35){
                    line=35;
                    if (isCancelled())return null;
                }
                else
                if (counterSecondLine==2 && line==35){
                    line=46;
                    if (isCancelled())return null;
                }

                else {


                    publishProgress(line++);}

                try {
                    Thread.sleep(2500);
                    if (isCancelled())return null;
                }
                catch (Exception e){
                    e.printStackTrace( );
                }

            }


            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {


            final TextView l2_1 = findViewById(R.id.l3_1);
            final     TextView l2_2 = findViewById(R.id.l3_2);
            final    TextView l2_3 = findViewById(R.id.l3_3);
            final    TextView  l2_4 = findViewById(R.id.l3_4);
            final    TextView l2_5  = findViewById(R.id.l3_5);
            final   Button b1l2  = findViewById(R.id.b1l3);
            final  Button b2l2  = findViewById(R.id.b2l3);
            final  TextView l2_6  = findViewById(R.id.l3_6);
            final  TextView l2_7  = findViewById(R.id.l3_7);
            final  TextView l2_8  = findViewById(R.id.l3_8);
            final  TextView l2_9  = findViewById(R.id.l3_9);
            final TextView  l2_10 = findViewById(R.id.l3_10);
            final  TextView l2_11  = findViewById(R.id.l3_11);
            final  TextView l2_12  = findViewById(R.id.l3_12);
            final  TextView l2_13  = findViewById(R.id.l3_13);
            final  TextView l2_14  = findViewById(R.id.l3_14);
            final  TextView l2_15  = findViewById(R.id.l3_15);
            final  TextView l2_16  = findViewById(R.id.l3_16);
            final TextView l2_17  = findViewById(R.id.l3_17);
            final TextView l2_18  = findViewById(R.id.l3_18);
            final TextView l2_19  = findViewById(R.id.l3_19);
            final TextView l2_20  = findViewById(R.id.l3_20);
            final TextView  l2_21 = findViewById(R.id.l3_21);
            final  TextView l2_22  = findViewById(R.id.l3_22);
            final   TextView l2_23  = findViewById(R.id.l3_23);
            final  TextView l2_24  = findViewById(R.id.l3_24);

            final TextView l2_25  = findViewById(R.id.l3_25);
            final  TextView l2_26  = findViewById(R.id.l3_26);
            final  TextView l2_27  = findViewById(R.id.l3_27);
            final  TextView l2_28  = findViewById(R.id.l3_28);
            final  TextView l2_29  = findViewById(R.id.l3_29);
            final  TextView l2_30  = findViewById(R.id.l3_30);
            final  TextView l2_31  = findViewById(R.id.l3_31);
            final  TextView l2_32  = findViewById(R.id.l3_32);
            final TextView l2_33  = findViewById(R.id.l3_33);
            final TextView l2_34  = findViewById(R.id.l3_34);
            final TextView l2_35  = findViewById(R.id.l3_35);
            final  TextView l2_36  = findViewById(R.id.l3_36);
            final TextView l2_37  = findViewById(R.id.l3_37);
            final TextView l2_38  = findViewById(R.id.l3_38);
            final TextView l2_39  = findViewById(R.id.l3_39);
            final TextView l2_40  = findViewById(R.id.l3_40);
            final TextView l2_41  = findViewById(R.id.l3_41);
            final TextView l2_42  = findViewById(R.id.l3_42);
            final  TextView l2_43  = findViewById(R.id.l3_43);
            final  Button b3l2  = findViewById(R.id.b3l3);
            final  Button b4l2  = findViewById(R.id.b4l3);
            final  TextView l2_44  = findViewById(R.id.l3_44);

            final   TextView l2_45  = findViewById(R.id.l3_45);
            final   TextView l2_46  = findViewById(R.id.l3_46);
            final  TextView l2_47  = findViewById(R.id.l3_47);
            final  TextView l2_48  = findViewById(R.id.l3_48);
            final  TextView l2_49  = findViewById(R.id.l3_49);
            final  TextView l2_50  = findViewById(R.id.l3_50);
            final  TextView l2_51  = findViewById(R.id.l3_51);
            final  TextView l2_52  = findViewById(R.id.l3_52);
            final   TextView l2_53  = findViewById(R.id.l3_53);
            final   TextView l2_54  = findViewById(R.id.l3_54);
            final  TextView l2_55 = findViewById(R.id.l3_55);
            final  TextView l2_56 = findViewById(R.id.l3_56);
            final TextView l2_57 = findViewById(R.id.l3_57);
            final TextView l2_58 = findViewById(R.id.l3_58);
            final TextView l2_59  = findViewById(R.id.l3_59);
            final  TextView l2_60  = findViewById(R.id.l3_60);
            final  TextView l2_61  = findViewById(R.id.l3_61);
            final TextView l2_62  = findViewById(R.id.l3_62);
            final  TextView l2_63  = findViewById(R.id.l3_63);
            final  TextView l2_64  = findViewById(R.id.l3_64);
            final  TextView l2_65  = findViewById(R.id.l3_65);
            final  TextView l2_66  = findViewById(R.id.l3_66);
            final  Button    b5l2  = findViewById(R.id.b5l3);


            final Animation a = AnimationUtils.loadAnimation(Level3.this, R.anim.text_in_game);

            switch (line){
                case 0: l2_1.setVisibility(View.VISIBLE); l2_1.startAnimation(a); break;
                case 1: l2_2.setVisibility(View.VISIBLE); l2_2.startAnimation(a); break;
                case 2: l2_3.setVisibility(View.VISIBLE); l2_3.startAnimation(a); break;
                case 3: l2_4.setVisibility(View.VISIBLE); l2_4.startAnimation(a); break;
                case 4: l2_5.setVisibility(View.VISIBLE); l2_5.startAnimation(a); break;
                case 5: b1l2.setVisibility(View.VISIBLE); b1l2.startAnimation(a);
                    b1l2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {
                                if (counterFirstLine==0){

                                    b1l2.setBackgroundResource(R.drawable.btn_vibor_l2);



                                    counterFirstLine=1;
                                } }
                            catch (Exception e){

                            }
                        }
                    });
                    b2l2.setVisibility(View.VISIBLE); b2l2.startAnimation(a);
                    b2l2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if (counterFirstLine==0){
                                    b2l2.setBackgroundResource(R.drawable.btn_vibor_l2);

                                    l2_6.setVisibility(View.GONE);
                                    l2_7.setVisibility(View.GONE);
                                    l2_8.setVisibility(View.GONE);
                                    l2_9.setVisibility(View.GONE);
                                    l2_10.setVisibility(View.GONE);
                                    l2_11.setVisibility(View.GONE);
                                    l2_12.setVisibility(View.GONE);
                                    l2_13.setVisibility(View.GONE);
                                    l2_14.setVisibility(View.GONE);

                                    line=15;
                                    counterFirstLine=1;
                                }}
                            catch (Exception e){

                            }
                        }
                    });
                    break;

                case 6: l2_6.setVisibility(View.VISIBLE); l2_6.startAnimation(a); break;
                case 7: l2_7.setVisibility(View.VISIBLE); l2_7.startAnimation(a); break;
                case 8: l2_8.setVisibility(View.VISIBLE); l2_8.startAnimation(a); break;
                case 9: l2_9.setVisibility(View.VISIBLE); l2_9.startAnimation(a); break;
                case 10: l2_10.setVisibility(View.VISIBLE); l2_10.startAnimation(a); break;
                case 11: l2_11.setVisibility(View.VISIBLE); l2_11.startAnimation(a); break;
                case 12: l2_12.setVisibility(View.VISIBLE); l2_12.startAnimation(a); break;
                case 13: l2_13.setVisibility(View.VISIBLE); l2_13.startAnimation(a); break;
                case 14: l2_14.setVisibility(View.VISIBLE); l2_14.startAnimation(a); break;
                case 15: l2_15.setVisibility(View.VISIBLE); l2_15.startAnimation(a); break;
                case 16: l2_16.setVisibility(View.VISIBLE); l2_16.startAnimation(a); break;
                case 17: l2_17.setVisibility(View.VISIBLE); l2_17.startAnimation(a); break;
                case 18: l2_18.setVisibility(View.VISIBLE); l2_18.startAnimation(a); break;
                case 19: l2_19.setVisibility(View.VISIBLE); l2_19.startAnimation(a); break;
                case 20: l2_20.setVisibility(View.VISIBLE); l2_20.startAnimation(a); break;
                case 21: l2_21.setVisibility(View.VISIBLE); l2_21.startAnimation(a); break;
                case 22: l2_22.setVisibility(View.VISIBLE); l2_22.startAnimation(a); break;
                case 23: l2_23.setVisibility(View.VISIBLE); l2_23.startAnimation(a); break;
                case 24: l2_24.setVisibility(View.VISIBLE); l2_24.startAnimation(a);
                  break;

                case 25: l2_25.setVisibility(View.VISIBLE); l2_25.startAnimation(a); break;
                case 26: l2_26.setVisibility(View.VISIBLE); l2_26.startAnimation(a); break;
                case 27: l2_27.setVisibility(View.VISIBLE); l2_27.startAnimation(a); break;
                case 28: l2_28.setVisibility(View.VISIBLE); l2_28.startAnimation(a); break;
                case 29: l2_29.setVisibility(View.VISIBLE); l2_29.startAnimation(a); break;
                case 30: l2_30.setVisibility(View.VISIBLE); l2_30.startAnimation(a); break;
                case 31: l2_31.setVisibility(View.VISIBLE); l2_31.startAnimation(a); break;
                case 32: l2_32.setVisibility(View.VISIBLE); l2_32.startAnimation(a); break;
                case 33: l2_33.setVisibility(View.VISIBLE); l2_33.startAnimation(a); break;
                case 34: l2_34.setVisibility(View.VISIBLE); l2_34.startAnimation(a); break;
                case 35:l2_35.setVisibility(View.VISIBLE); l2_35.startAnimation(a);


                    b3l2.setVisibility(View.VISIBLE); b3l2.startAnimation(a);
                    b3l2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if(counterSecondLine==0){
                                    b3l2.setBackgroundResource(R.drawable.btn_vibor_l2);
                                    line=35;
                                    counterSecondLine=1;
                                }

                            }
                            catch (Exception e){

                            }
                        }
                    });


                    b4l2.setVisibility(View.VISIBLE); b4l2.startAnimation(a);
                    b4l2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if(counterSecondLine==0){
                                    b4l2.setBackgroundResource(R.drawable.btn_vibor_l2);




                                    l2_36.setVisibility(View.GONE);
                                    l2_37.setVisibility(View.GONE);
                                    l2_38.setVisibility(View.GONE);
                                    l2_39.setVisibility(View.GONE);
                                    l2_40.setVisibility(View.GONE);
                                    l2_41.setVisibility(View.GONE);
                                    l2_42.setVisibility(View.GONE);
                                    l2_43.setVisibility(View.GONE);
                                    l2_44.setVisibility(View.GONE);
                                    l2_45.setVisibility(View.GONE);
                                    counterSecondLine=2;
                                       line=46;
                                }}
                            catch (Exception e){

                            }
                        }
                    });
                    break;

















                case 36:

                l2_36.setVisibility(View.VISIBLE); l2_36.startAnimation(a); break;


                case 37: l2_37.setVisibility(View.VISIBLE); l2_37.startAnimation(a); break;
                case 38: l2_38.setVisibility(View.VISIBLE); l2_38.startAnimation(a); break;
                case 39: l2_39.setVisibility(View.VISIBLE); l2_39.startAnimation(a); break;
                case 40: l2_40.setVisibility(View.VISIBLE); l2_40.startAnimation(a); break;
                case 41: l2_41.setVisibility(View.VISIBLE); l2_41.startAnimation(a); break;
                case 42: l2_42.setVisibility(View.VISIBLE); l2_42.startAnimation(a); break;
                case 43: l2_43.setVisibility(View.VISIBLE); l2_43.startAnimation(a); break;
                case 44: l2_44.setVisibility(View.VISIBLE); l2_44.startAnimation(a); break;
                case 45:

                    break;
                case 46: l2_45.setVisibility(View.VISIBLE); l2_45.startAnimation(a); break;
                case 47: l2_46.setVisibility(View.VISIBLE); l2_46.startAnimation(a); break;
                case 48: l2_47.setVisibility(View.VISIBLE); l2_47.startAnimation(a); break;
                case 49: l2_48.setVisibility(View.VISIBLE); l2_48.startAnimation(a); break;
                case 50: l2_49.setVisibility(View.VISIBLE); l2_49.startAnimation(a); break;
                case 51: l2_50.setVisibility(View.VISIBLE); l2_50.startAnimation(a); break;
                case 52: l2_51.setVisibility(View.VISIBLE); l2_51.startAnimation(a); break;
                case 53: l2_52.setVisibility(View.VISIBLE); l2_52.startAnimation(a); break;
                case 54: l2_53.setVisibility(View.VISIBLE); l2_53.startAnimation(a); break;
                case 55: l2_54.setVisibility(View.VISIBLE); l2_54.startAnimation(a); break;
                case 56: l2_55.setVisibility(View.VISIBLE); l2_55.startAnimation(a); break;
                case 57: l2_56.setVisibility(View.VISIBLE); l2_56.startAnimation(a); break;
                case 58: l2_57.setVisibility(View.VISIBLE); l2_57.startAnimation(a); break;
                case 59: l2_58.setVisibility(View.VISIBLE); l2_58.startAnimation(a); break;
                case 60: l2_59.setVisibility(View.VISIBLE); l2_59.startAnimation(a); break;
                case 61: l2_60.setVisibility(View.VISIBLE); l2_60.startAnimation(a); break;
                case 62: l2_61.setVisibility(View.VISIBLE); l2_61.startAnimation(a); break;
                case 63: l2_62.setVisibility(View.VISIBLE); l2_62.startAnimation(a); break;
                case 64: l2_63.setVisibility(View.VISIBLE); l2_63.startAnimation(a); break;
                case 65: l2_64.setVisibility(View.VISIBLE); l2_64.startAnimation(a); break;
                case 66: l2_65.setVisibility(View.VISIBLE); l2_65.startAnimation(a); l2_66.setVisibility(View.VISIBLE); l2_66.startAnimation(a); break;
                case 67: b5l2.setVisibility(View.VISIBLE); b5l2.startAnimation(a);
                    b5l2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                b5l2.setBackgroundResource(R.drawable.btn_vibor_l2);
                                    Intent lv4 = new Intent(Level3.this, Preview_Lvl4.class);
                                    startActivity(lv4);
                                    finish();

                            }
                            catch (Exception e){

                            }
                        }
                    });

                    break;
                default:break;




            }

        }
    }
    @Override

    public void onBackPressed() {

        super.onBackPressed();

    }

}




