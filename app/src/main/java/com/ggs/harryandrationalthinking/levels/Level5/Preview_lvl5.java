package com.ggs.harryandrationalthinking.levels.Level5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.ggs.harryandrationalthinking.R;
import com.ggs.harryandrationalthinking.levels.Level4.Level4;
import com.ggs.harryandrationalthinking.levels.Level4.Preview_Lvl4;

public class Preview_lvl5 extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGHT = 3500;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_lvl5);
        new Handler().postDelayed(new Runnable() {

            @Override

            public void run() {

                Intent mainIntent = new Intent(Preview_lvl5.this, Level5.class);

                Preview_lvl5.this.startActivity(mainIntent);

                Preview_lvl5.this.finish();

            }

        }, SPLASH_DISPLAY_LENGHT);

    }



    @Override

    public void onBackPressed() {

        super.onBackPressed();

    }

}