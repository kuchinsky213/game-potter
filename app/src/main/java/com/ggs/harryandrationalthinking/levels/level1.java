package com.ggs.harryandrationalthinking.levels;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.ggs.harryandrationalthinking.R;
import com.ggs.harryandrationalthinking.data.OneTable;

import java.util.ArrayList;

public class level1 extends AppCompatActivity {
    Button dalee;
    TextView tv0_1, tv0_2, tv0_3, tv0_4, tv0_5;
    OneTable oneTable = new OneTable();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_level1);
        SharedPreferences save = getSharedPreferences("Save", MODE_PRIVATE);
        SharedPreferences.Editor editor = save.edit();
        editor.putInt("Level", 0);
        editor.commit();
        final Animation text_game = AnimationUtils.loadAnimation(level1.this, R.anim.text_in_game);

        ArrayList<TextView> arrtextView = new ArrayList<TextView>();

        tv0_1 = findViewById(R.id.tv0_1);
        tv0_2 = findViewById(R.id.tv0_2);
        tv0_3 = findViewById(R.id.tv0_3);
        tv0_4 = findViewById(R.id.tv0_4);
        tv0_5 = findViewById(R.id.tv0_5);
        dalee = findViewById(R.id.buttonN);

//-------------------------------------------------------------------------------
        arrtextView.add(tv0_1);
        arrtextView.add(tv0_2);
        arrtextView.add(tv0_3);
        arrtextView.add(tv0_4);
        arrtextView.add(tv0_5);

        int i;

//----------zapolnyayu text
        for (i = 0; i < arrtextView.size(); i++) {
            arrtextView.get(i).setText(oneTable.onescenario_ru[i]);
        }



        for (i = 0; i < arrtextView.size(); i++) {
            arrtextView.get(i).setAnimation(text_game);
            arrtextView.get(i).setVisibility(View.VISIBLE);
            dalee.setVisibility(View.VISIBLE);
        }

        dalee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lv1 = new Intent(level1.this, Level1_1.class);
                startActivity(lv1);
                finish();
            }
        });

    }
}