package com.ggs.harryandrationalthinking.levels;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ggs.harryandrationalthinking.MainActivity;
import com.ggs.harryandrationalthinking.R;
import com.ggs.harryandrationalthinking.data.OneTableOne;
import com.ggs.harryandrationalthinking.levels.level2.Preview;

public class Level1_1 extends AppCompatActivity {
OneTableOne twotable = new OneTableOne();
TextView l1_1_0,l1_1_1,
        l1_1_2, l1_1_3, l1_1_4, l1_1_5,  l1_1_6, l1_1_7, l1_1_8, l1_1_9, l1_1_10, l1_1_11, l1_1_12, l1_1_13, l1_1_14, l1_1_15, l1_1_16, l1_1_17, l1_1_18, l1_1_19, l1_1_20, l1_1_21, l1_1_22,
        l1_1_23, l1_1_24, l1_1_25, l1_1_26, l1_1_27, l1_1_28, l1_1_29, l1_1_30, l1_1_31, l1_1_32, l1_1_33, l1_1_34, l1_1_35, l1_1_36, l1_1_37, l1_1_38, l1_1_39, l1_1_40, l1_1_41, l1_1_42, l1_1_43,
        l1_1_44, l1_1_45, l1_1_46, l1_1_47, l1_1_48, l1_1_49, l1_1_50, l1_1_51, l1_1_52, l1_1_53, l1_1_54, l1_1_55, l1_1_56, l1_1_57, l1_1_58, l1_1_59, l1_1_60, l1_1_61, l1_1_62, l1_1_63, l1_1_64,
        l1_1_65, l1_1_66,  l1_1_67,   l1_1_68,  l1_1_69,  l1_1_70,  l1_1_71,  l1_1_72;
TextView vsrup;
Button button0, button2, button3, button4, button5, button6, button7;
ImageView home_lvl1, harry;
public Animation a;
public int line = -1;
public int counterFirstLine = 0;
public int counterSecondLine = 0;
public int counterThirdLine = 0;
public int secondYes = 0;
    Delay delay = new Delay();

    @Override
    public void onBackPressed() {
delay.cancel(true);
delay=null;
        Intent main = new Intent(Level1_1.this, MainActivity.class);
        startActivity(main);
       finish();



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level1_1);
        SharedPreferences save = getSharedPreferences("Save", MODE_PRIVATE);
        SharedPreferences.Editor editor = save.edit();
        editor.putInt("Level", 1);
        editor.commit();


TextView vsrup = findViewById(R.id.vstup);
        ImageView home_lvl1 = findViewById(R.id.home_lvl1);
        ImageView harry = findViewById(R.id.harry_lvl_1);


        final Button button0 = findViewById(R.id.button);
        final Button button2 = findViewById(R.id.button2);
        final Button button3 = findViewById(R.id.button3);
        final Button button4 = findViewById(R.id.button4);
        final Button button5 = findViewById(R.id.button5);
        final Button button6 = findViewById(R.id.button6);
        final Button button7 = findViewById(R.id.button7);



        final TextView l1_1_0 = findViewById(R.id.l1_1_0);
        final TextView l1_1_1 = findViewById(R.id.l1_1_1);
        final TextView l1_1_2 = findViewById(R.id.l1_1_2);
        final TextView l1_1_3 = findViewById(R.id.l1_1_3);
        final TextView l1_1_4 = findViewById(R.id.l1_1_4);
        final TextView l1_1_5 = findViewById(R.id.l1_1_5);
        final TextView l1_1_6 = findViewById(R.id.l1_1_6);
        final TextView l1_1_7 = findViewById(R.id.l1_1_7);
        final TextView l1_1_8 = findViewById(R.id.l1_1_8);
        final TextView l1_1_9 = findViewById(R.id.l1_1_9);
        final TextView l1_1_10 = findViewById(R.id.l1_1_10);
        final TextView l1_1_11 = findViewById(R.id.l1_1_11);
        final TextView l1_1_12 = findViewById(R.id.l1_1_12);
        final TextView l1_1_13 = findViewById(R.id.l1_1_13);
        final TextView l1_1_14 = findViewById(R.id.l1_1_14);
        final TextView l1_1_15 = findViewById(R.id.l1_1_15); //первый вариант
        final TextView l1_1_16 = findViewById(R.id.l1_1_16); //первый вариант
        final TextView l1_1_17 = findViewById(R.id.l1_1_17); //второй
        final TextView l1_1_18 = findViewById(R.id.l1_1_18); //второй
        final TextView l1_1_19 = findViewById(R.id.l1_1_19); //первый вариант
        final TextView l1_1_20 = findViewById(R.id.l1_1_20);
        final TextView l1_1_21 = findViewById(R.id.l1_1_21);
        final TextView l1_1_22 = findViewById(R.id.l1_1_22);
        final TextView l1_1_23 = findViewById(R.id.l1_1_23);
        final TextView l1_1_24 = findViewById(R.id.l1_1_24);
        final TextView l1_1_25 = findViewById(R.id.l1_1_25);
        final TextView l1_1_26 = findViewById(R.id.l1_1_26);  //первый вариант
        final TextView l1_1_27 = findViewById(R.id.l1_1_27); //второй
        final TextView l1_1_28 = findViewById(R.id.l1_1_28);  //первый вариант
        final TextView l1_1_29 = findViewById(R.id.l1_1_29);  //первый вариант
        final TextView l1_1_30 = findViewById(R.id.l1_1_30); //второй
        final TextView l1_1_31 = findViewById(R.id.l1_1_31);
        final TextView l1_1_32 = findViewById(R.id.l1_1_32);
        final TextView l1_1_33 = findViewById(R.id.l1_1_33);
        final TextView l1_1_34 = findViewById(R.id.l1_1_34);
        final TextView l1_1_35 = findViewById(R.id.l1_1_35);
        final TextView l1_1_36 = findViewById(R.id.l1_1_36);
        final TextView l1_1_37 = findViewById(R.id.l1_1_37);
        final TextView l1_1_38 = findViewById(R.id.l1_1_38);
        final TextView l1_1_39 = findViewById(R.id.l1_1_39);
        final TextView l1_1_40 = findViewById(R.id.l1_1_40);
        final TextView l1_1_41 = findViewById(R.id.l1_1_41);
        final TextView l1_1_42 = findViewById(R.id.l1_1_42);
        final TextView l1_1_43 = findViewById(R.id.l1_1_43);
        final TextView l1_1_44 = findViewById(R.id.l1_1_44);
        final TextView l1_1_45 = findViewById(R.id.l1_1_45); //первый вариант
        final TextView l1_1_46 = findViewById(R.id.l1_1_46); //первый вариант
        final TextView l1_1_47 = findViewById(R.id.l1_1_47); //первый вариант
        final TextView l1_1_48 = findViewById(R.id.l1_1_48); //первый вариант
        final TextView l1_1_49 = findViewById(R.id.l1_1_49); //первый вариант и второй
        final TextView l1_1_50 = findViewById(R.id.l1_1_50); //первый вариант и второй
        final TextView l1_1_51 = findViewById(R.id.l1_1_51);
        final TextView l1_1_52 = findViewById(R.id.l1_1_52);
        final TextView l1_1_53 = findViewById(R.id.l1_1_53);
        final TextView l1_1_54 = findViewById(R.id.l1_1_54);
        final TextView l1_1_55 = findViewById(R.id.l1_1_55);
        final TextView l1_1_56 = findViewById(R.id.l1_1_56);
        final TextView l1_1_57 = findViewById(R.id.l1_1_57);
        final TextView l1_1_58 = findViewById(R.id.l1_1_58);
        final TextView l1_1_59 = findViewById(R.id.l1_1_59);
        final TextView l1_1_60 = findViewById(R.id.l1_1_60);
        final TextView l1_1_61 = findViewById(R.id.l1_1_61);
        final TextView l1_1_62 = findViewById(R.id.l1_1_62);
        final TextView l1_1_63 = findViewById(R.id.l1_1_63);
        final TextView l1_1_64 = findViewById(R.id.l1_1_64);
        final TextView l1_1_65 = findViewById(R.id.l1_1_65);
        final TextView l1_1_66 = findViewById(R.id.l1_1_66);


        final TextView   l1_1_67 = findViewById(R.id.l1_1_67);
        final TextView    l1_1_68 = findViewById(R.id.l1_1_68);
        final TextView   l1_1_69= findViewById(R.id.l1_1_69);
        final TextView  l1_1_70= findViewById(R.id.l1_1_70);
        final TextView  l1_1_71= findViewById(R.id.l1_1_71);
        final TextView l1_1_72= findViewById(R.id.l1_1_72);
//----------zapolnyayu

        l1_1_0.setText(twotable.onescenarioone_ru[0]);
        l1_1_1.setText(twotable.onescenarioone_ru[1]);
        l1_1_2.setText(twotable.onescenarioone_ru[2]);
        l1_1_3.setText(twotable.onescenarioone_ru[3]);
        l1_1_4.setText(twotable.onescenarioone_ru[4]);
        l1_1_5.setText(twotable.onescenarioone_ru[5]);
        l1_1_6.setText(twotable.onescenarioone_ru[6]);
        l1_1_7.setText(twotable.onescenarioone_ru[7]);
        l1_1_8.setText(twotable.onescenarioone_ru[8]);
        l1_1_9.setText(twotable.onescenarioone_ru[9]);
        l1_1_10.setText(twotable.onescenarioone_ru[10]);
        l1_1_11.setText(twotable.onescenarioone_ru[11]);
        l1_1_12.setText(twotable.onescenarioone_ru[12]);
        l1_1_13.setText(twotable.onescenarioone_ru[13]);
        l1_1_14.setText(twotable.onescenarioone_ru[14]);
        l1_1_15.setText(twotable.onescenarioone_ru[15]);
        l1_1_16.setText(twotable.onescenarioone_ru[16]);
        l1_1_17.setText(twotable.onescenarioone_ru[17]);
        l1_1_18.setText(twotable.onescenarioone_ru[18]);
        l1_1_19.setText(twotable.onescenarioone_ru[19]);
        l1_1_20.setText(twotable.onescenarioone_ru[20]);
        l1_1_21.setText(twotable.onescenarioone_ru[21]);
        l1_1_22.setText(twotable.onescenarioone_ru[22]);
        l1_1_23.setText(twotable.onescenarioone_ru[23]);
        l1_1_24.setText(twotable.onescenarioone_ru[24]);
        l1_1_25.setText(twotable.onescenarioone_ru[25]);
        l1_1_26.setText(twotable.onescenarioone_ru[26]);
        l1_1_27.setText(twotable.onescenarioone_ru[27]);
        l1_1_28.setText(twotable.onescenarioone_ru[28]);
        l1_1_29.setText(twotable.onescenarioone_ru[29]);
        l1_1_30.setText(twotable.onescenarioone_ru[30]);
        l1_1_31.setText(twotable.onescenarioone_ru[31]);
        l1_1_32.setText(twotable.onescenarioone_ru[32]);
        l1_1_33.setText(twotable.onescenarioone_ru[33]);
        l1_1_34.setText(twotable.onescenarioone_ru[34]);
        l1_1_35.setText(twotable.onescenarioone_ru[35]);
        l1_1_36.setText(twotable.onescenarioone_ru[36]);
        l1_1_37.setText(twotable.onescenarioone_ru[37]);
        l1_1_38.setText(twotable.onescenarioone_ru[38]);
        l1_1_39.setText(twotable.onescenarioone_ru[39]);
        l1_1_40.setText(twotable.onescenarioone_ru[40]);
        l1_1_41.setText(twotable.onescenarioone_ru[41]);
        l1_1_42.setText(twotable.onescenarioone_ru[42]);
        l1_1_43.setText(twotable.onescenarioone_ru[43]);
        l1_1_44.setText(twotable.onescenarioone_ru[44]);
        l1_1_45.setText(twotable.onescenarioone_ru[45]);
        l1_1_46.setText(twotable.onescenarioone_ru[46]);
        l1_1_47.setText(twotable.onescenarioone_ru[47]);
        l1_1_48.setText(twotable.onescenarioone_ru[48]);
        l1_1_49.setText(twotable.onescenarioone_ru[49]);
        l1_1_50.setText(twotable.onescenarioone_ru[50]);
        l1_1_51.setText(twotable.onescenarioone_ru[51]);
        l1_1_52.setText(twotable.onescenarioone_ru[52]);
        l1_1_53.setText(twotable.onescenarioone_ru[53]);
        l1_1_54.setText(twotable.onescenarioone_ru[54]);
        l1_1_55.setText(twotable.onescenarioone_ru[55]);
        l1_1_56.setText(twotable.onescenarioone_ru[56]);
        l1_1_57.setText(twotable.onescenarioone_ru[57]);
        l1_1_58.setText(twotable.onescenarioone_ru[58]);
        l1_1_59.setText(twotable.onescenarioone_ru[59]);
        l1_1_60.setText(twotable.onescenarioone_ru[60]);
        l1_1_61.setText(twotable.onescenarioone_ru[61]);
        l1_1_62.setText(twotable.onescenarioone_ru[62]);
        l1_1_63.setText(twotable.onescenarioone_ru[63]);
        l1_1_64.setText(twotable.onescenarioone_ru[64]);
        l1_1_65.setText(twotable.onescenarioone_ru[65]);
        l1_1_66.setText(twotable.onescenarioone_ru[66]);
        l1_1_67.setText(twotable.onescenarioone_ru[67]);
        l1_1_68.setText(twotable.onescenarioone_ru[68]);
        l1_1_69.setText(twotable.onescenarioone_ru[69]);
        l1_1_70.setText(twotable.onescenarioone_ru[70]);
        l1_1_71.setText(twotable.onescenarioone_ru[71]);
        l1_1_72.setText(twotable.onescenarioone_ru[72]);




                l1_1_0.setVisibility(View.INVISIBLE);
                l1_1_1.setVisibility(View.INVISIBLE);
                l1_1_2.setVisibility(View.INVISIBLE);
                l1_1_3.setVisibility(View.INVISIBLE);
                l1_1_4.setVisibility(View.INVISIBLE);
                l1_1_5.setVisibility(View.INVISIBLE);
                l1_1_6.setVisibility(View.INVISIBLE);
                l1_1_7.setVisibility(View.INVISIBLE);
                l1_1_8.setVisibility(View.INVISIBLE);
                l1_1_9.setVisibility(View.INVISIBLE);
                l1_1_10.setVisibility(View.INVISIBLE);
                l1_1_11.setVisibility(View.INVISIBLE);
                l1_1_12.setVisibility(View.INVISIBLE);
                l1_1_13.setVisibility(View.INVISIBLE);
                l1_1_14.setVisibility(View.INVISIBLE);
                l1_1_15.setVisibility(View.GONE);
                l1_1_16.setVisibility(View.GONE);
                l1_1_17.setVisibility(View.GONE);
                l1_1_18.setVisibility(View.GONE);
                l1_1_19.setVisibility(View.GONE);
                l1_1_20.setVisibility(View.INVISIBLE);
                l1_1_21.setVisibility(View.INVISIBLE);
                l1_1_22.setVisibility(View.INVISIBLE);

                l1_1_23.setVisibility(View.INVISIBLE);
                l1_1_24.setVisibility(View.INVISIBLE);
                l1_1_25.setVisibility(View.INVISIBLE);
                l1_1_26.setVisibility(View.GONE);
                l1_1_27.setVisibility(View.GONE);
                l1_1_28.setVisibility(View.GONE);
                l1_1_29.setVisibility(View.GONE);
                l1_1_30.setVisibility(View.GONE);
                l1_1_31.setVisibility(View.INVISIBLE);
                l1_1_32.setVisibility(View.INVISIBLE);
                l1_1_33.setVisibility(View.INVISIBLE);
                l1_1_34.setVisibility(View.INVISIBLE);
                l1_1_35.setVisibility(View.INVISIBLE);
                l1_1_36.setVisibility(View.INVISIBLE);
                l1_1_37.setVisibility(View.INVISIBLE);
                l1_1_38.setVisibility(View.INVISIBLE);
                l1_1_39.setVisibility(View.INVISIBLE);
                l1_1_40.setVisibility(View.INVISIBLE);
                l1_1_41.setVisibility(View.INVISIBLE);
                l1_1_42.setVisibility(View.INVISIBLE);
                l1_1_43.setVisibility(View.INVISIBLE);
                l1_1_44.setVisibility(View.INVISIBLE);
                l1_1_45.setVisibility(View.GONE);
                l1_1_46.setVisibility(View.GONE);
                l1_1_47.setVisibility(View.GONE);
                l1_1_48.setVisibility(View.GONE);
                l1_1_49.setVisibility(View.GONE);
                l1_1_50.setVisibility(View.GONE);
                l1_1_51.setVisibility(View.INVISIBLE);
                l1_1_52.setVisibility(View.INVISIBLE);
                l1_1_53.setVisibility(View.INVISIBLE);
                l1_1_54.setVisibility(View.INVISIBLE);
                l1_1_55.setVisibility(View.INVISIBLE);
                l1_1_56.setVisibility(View.INVISIBLE);
                l1_1_57.setVisibility(View.INVISIBLE);
                l1_1_58.setVisibility(View.INVISIBLE);
                l1_1_59.setVisibility(View.INVISIBLE);
                l1_1_60.setVisibility(View.INVISIBLE);
                l1_1_61.setVisibility(View.INVISIBLE);
                l1_1_62.setVisibility(View.INVISIBLE);
                l1_1_63.setVisibility(View.INVISIBLE);
                l1_1_64.setVisibility(View.INVISIBLE);

                l1_1_65.setVisibility(View.INVISIBLE);
                l1_1_66.setVisibility(View.INVISIBLE);
                l1_1_67.setVisibility(View.INVISIBLE);
                l1_1_68.setVisibility(View.INVISIBLE);
                l1_1_69.setVisibility(View.INVISIBLE);
                l1_1_70.setVisibility(View.INVISIBLE);

                        l1_1_71.setVisibility(View.INVISIBLE);
                        l1_1_72.setVisibility(View.INVISIBLE);

                        home_lvl1.setVisibility(View.INVISIBLE);
                        harry.setVisibility(View.INVISIBLE);
        vsrup.setVisibility(View.INVISIBLE);

        button0.setVisibility(View.INVISIBLE);  button2.setVisibility(View.INVISIBLE);

        button3.setVisibility(View.INVISIBLE);  button4.setVisibility(View.INVISIBLE);

        button5.setVisibility(View.INVISIBLE);  button6.setVisibility(View.INVISIBLE);

        button7.setVisibility(View.INVISIBLE);
delay.execute();
    }

    public class Delay extends AsyncTask<Void, Integer, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            while (line <=76) {
                if(counterFirstLine == 0 && line==15){
                    line=15;
                    if (isCancelled())return null;
                } else{
                    if (counterFirstLine == 2 && line==15) {

                        line = 19;
                        if (isCancelled())return null;
                    }
                    else{
                        if (counterSecondLine==0 && line == 27){
                            line=27;
                            if (isCancelled())return null;
                    }

                    else {
                        if (counterSecondLine==2 && line == 27){
                            line=32;
                            if (isCancelled())return null;
                        }

                        else {
                            if (counterThirdLine==0 && line == 47){

                                 line = 47;
                                if (isCancelled())return null;
                            }
                            else {    if(counterThirdLine==2 && line == 47){
                            line=52;
                                if (isCancelled())return null;
                        }
                        else
                        {




                            publishProgress(line = line + 1);
                            try {
                                Thread.sleep(2500);
                                if (isCancelled())return null;
                            } catch (Exception e) {
                                e.printStackTrace();

                            }} }}
                }
            }}}
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            TextView vsrup = findViewById(R.id.vstup);
            ImageView home_lvl1 = findViewById(R.id.home_lvl1);
            ImageView harry = findViewById(R.id.harry_lvl_1);

            final Button button0 = findViewById(R.id.button);
            final Button button2 = findViewById(R.id.button2);
            final Button button3 = findViewById(R.id.button3);
            final Button button4 = findViewById(R.id.button4);
            final Button button5 = findViewById(R.id.button5);
            final Button button6 = findViewById(R.id.button6);
            final Button button7 = findViewById(R.id.button7);



            final TextView l1_1_0 = findViewById(R.id.l1_1_0);
            final TextView l1_1_1 = findViewById(R.id.l1_1_1);
            final TextView l1_1_2 = findViewById(R.id.l1_1_2);
            final TextView l1_1_3 = findViewById(R.id.l1_1_3);
            final TextView l1_1_4 = findViewById(R.id.l1_1_4);
            final TextView l1_1_5 = findViewById(R.id.l1_1_5);
            final TextView l1_1_6 = findViewById(R.id.l1_1_6);
            final TextView l1_1_7 = findViewById(R.id.l1_1_7);
            final TextView l1_1_8 = findViewById(R.id.l1_1_8);
            final TextView l1_1_9 = findViewById(R.id.l1_1_9);
            final TextView l1_1_10 = findViewById(R.id.l1_1_10);
            final TextView l1_1_11 = findViewById(R.id.l1_1_11);
            final TextView l1_1_12 = findViewById(R.id.l1_1_12);
            final TextView l1_1_13 = findViewById(R.id.l1_1_13);
            final TextView l1_1_14 = findViewById(R.id.l1_1_14);
            final TextView l1_1_15 = findViewById(R.id.l1_1_15); //первый вариант
            final TextView l1_1_16 = findViewById(R.id.l1_1_16); //первый вариант
            final TextView l1_1_17 = findViewById(R.id.l1_1_17); //второй
            final TextView l1_1_18 = findViewById(R.id.l1_1_18); //второй
            final TextView l1_1_19 = findViewById(R.id.l1_1_19); //первый вариант
            final TextView l1_1_20 = findViewById(R.id.l1_1_20);
            final TextView l1_1_21 = findViewById(R.id.l1_1_21);
            final TextView l1_1_22 = findViewById(R.id.l1_1_22);
            final TextView l1_1_23 = findViewById(R.id.l1_1_23);
            final TextView l1_1_24 = findViewById(R.id.l1_1_24);
            final TextView l1_1_25 = findViewById(R.id.l1_1_25);
            final TextView l1_1_26 = findViewById(R.id.l1_1_26);  //первый вариант
            final TextView l1_1_27 = findViewById(R.id.l1_1_27); //второй
            final TextView l1_1_28 = findViewById(R.id.l1_1_28);  //первый вариант
            final TextView l1_1_29 = findViewById(R.id.l1_1_29);  //первый вариант
            final TextView l1_1_30 = findViewById(R.id.l1_1_30); //второй
            final TextView l1_1_31 = findViewById(R.id.l1_1_31);
            final TextView l1_1_32 = findViewById(R.id.l1_1_32);
            final TextView l1_1_33 = findViewById(R.id.l1_1_33);
            final TextView l1_1_34 = findViewById(R.id.l1_1_34);
            final TextView l1_1_35 = findViewById(R.id.l1_1_35);
            final TextView l1_1_36 = findViewById(R.id.l1_1_36);
            final TextView l1_1_37 = findViewById(R.id.l1_1_37);
            final TextView l1_1_38 = findViewById(R.id.l1_1_38);
            final TextView l1_1_39 = findViewById(R.id.l1_1_39);
            final TextView l1_1_40 = findViewById(R.id.l1_1_40);
            final TextView l1_1_41 = findViewById(R.id.l1_1_41);
            final TextView l1_1_42 = findViewById(R.id.l1_1_42);
            final TextView l1_1_43 = findViewById(R.id.l1_1_43);
            final TextView l1_1_44 = findViewById(R.id.l1_1_44);
            final TextView l1_1_45 = findViewById(R.id.l1_1_45); //первый вариант
            final TextView l1_1_46 = findViewById(R.id.l1_1_46); //первый вариант
            final TextView l1_1_47 = findViewById(R.id.l1_1_47); //первый вариант
            final TextView l1_1_48 = findViewById(R.id.l1_1_48); //первый вариант
            final TextView l1_1_49 = findViewById(R.id.l1_1_49); //первый вариант и второй
            final TextView l1_1_50 = findViewById(R.id.l1_1_50); //первый вариант и второй
            final TextView l1_1_51 = findViewById(R.id.l1_1_51);
            final TextView l1_1_52 = findViewById(R.id.l1_1_52);
            final TextView l1_1_53 = findViewById(R.id.l1_1_53);
            final TextView l1_1_54 = findViewById(R.id.l1_1_54);
            final TextView l1_1_55 = findViewById(R.id.l1_1_55);
            final TextView l1_1_56 = findViewById(R.id.l1_1_56);
            final TextView l1_1_57 = findViewById(R.id.l1_1_57);
            final TextView l1_1_58 = findViewById(R.id.l1_1_58);
            final TextView l1_1_59 = findViewById(R.id.l1_1_59);
            final TextView l1_1_60 = findViewById(R.id.l1_1_60);
            final TextView l1_1_61 = findViewById(R.id.l1_1_61);
            final TextView l1_1_62 = findViewById(R.id.l1_1_62);
            final TextView l1_1_63 = findViewById(R.id.l1_1_63);
            final TextView l1_1_64 = findViewById(R.id.l1_1_64);
            final TextView l1_1_65 = findViewById(R.id.l1_1_65);
            final TextView l1_1_66 = findViewById(R.id.l1_1_66);


            final TextView   l1_1_67 = findViewById(R.id.l1_1_67);
            final TextView    l1_1_68 = findViewById(R.id.l1_1_68);
            final TextView   l1_1_69= findViewById(R.id.l1_1_69);
            final TextView  l1_1_70= findViewById(R.id.l1_1_70);
            final TextView  l1_1_71= findViewById(R.id.l1_1_71);
            final TextView l1_1_72= findViewById(R.id.l1_1_72);





            final  Animation a = AnimationUtils.loadAnimation(Level1_1.this, R.anim.text_in_game);
            switch (line){
                case 0:        vsrup.setVisibility(View.VISIBLE); vsrup.startAnimation(a);
                    l1_1_0.setVisibility(View.VISIBLE); l1_1_0.startAnimation(a); break;

                case 1:  l1_1_1.setVisibility(View.VISIBLE); l1_1_1.startAnimation(a); break;
                case 2:   l1_1_2.setVisibility(View.VISIBLE); l1_1_2.startAnimation(a);
                         home_lvl1.setVisibility(View.VISIBLE); home_lvl1.startAnimation(a); break;
                case 3: l1_1_3.setVisibility(View.VISIBLE); l1_1_3.startAnimation(a); break;
                case 4:  l1_1_4.setVisibility(View.VISIBLE); l1_1_4.startAnimation(a); break;
                case 5:  l1_1_5.setVisibility(View.VISIBLE); l1_1_5.startAnimation(a); break;
                case 6:  l1_1_6.setVisibility(View.VISIBLE); l1_1_6.startAnimation(a); break;
                case 7: l1_1_7.setVisibility(View.VISIBLE); l1_1_7.startAnimation(a); break;
                case 8: l1_1_8.setVisibility(View.VISIBLE); l1_1_8.startAnimation(a); break;
                case 9:  l1_1_9.setVisibility(View.VISIBLE); l1_1_9.startAnimation(a); break;
                case 10:  l1_1_10.setVisibility(View.VISIBLE); l1_1_10.startAnimation(a); break;
                case 11:  l1_1_11.setVisibility(View.VISIBLE); l1_1_11.startAnimation(a); break;
                case 12: l1_1_12.setVisibility(View.VISIBLE); l1_1_12.startAnimation(a); break;
                case 13: l1_1_13.setVisibility(View.VISIBLE); l1_1_13.startAnimation(a); break;
                case 14: l1_1_14.setVisibility(View.VISIBLE); l1_1_14.startAnimation(a); break;
                case 15: button0.setVisibility(View.VISIBLE); button0.startAnimation(a);
                button0.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (counterFirstLine==0){
                            button0.setBackgroundResource(R.drawable.btn_vibor_pressed);

                            counterFirstLine=1;}
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                    button2.setVisibility(View.VISIBLE); button2.startAnimation(a);
                    button2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {  if (counterFirstLine==0){
                                button2.setBackgroundResource(R.drawable.btn_vibor_pressed);
                                l1_1_17.setVisibility(View.VISIBLE);
                                l1_1_18.setVisibility(View.VISIBLE);
                                counterFirstLine=2;
                                line=19;}
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
break;
                case 16: l1_1_15.setVisibility(View.VISIBLE);


                    break;

                case 17:       l1_1_16.setVisibility(View.VISIBLE); l1_1_16.startAnimation(a);

                    l1_1_18.setVisibility(View.GONE);
                    l1_1_19.setVisibility(View.GONE);

                    line = 20;
                break;
                case 18:       l1_1_17.setVisibility(View.VISIBLE); l1_1_17.startAnimation(a); break;
                case 19:        l1_1_18.setVisibility(View.VISIBLE); l1_1_18.startAnimation(a); break;
                case 20:        l1_1_19.setVisibility(View.VISIBLE); l1_1_19.startAnimation(a); break;
                case 21:       l1_1_20.setVisibility(View.VISIBLE); l1_1_20.startAnimation(a); break;
                case 22:       l1_1_21.setVisibility(View.VISIBLE); l1_1_21.startAnimation(a); break;
                case 23:      l1_1_22.setVisibility(View.VISIBLE); l1_1_22.startAnimation(a); break;

                case 24:    l1_1_23.setVisibility(View.VISIBLE); l1_1_23.startAnimation(a); break;
                case 25:      l1_1_24.setVisibility(View.VISIBLE); l1_1_24.startAnimation(a); break;
                case 26:     l1_1_25.setVisibility(View.VISIBLE); l1_1_25.startAnimation(a); break;
                case 27: button3.setVisibility(View.VISIBLE); button3.startAnimation(a);
                button3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (counterSecondLine==0) {
                                button3.setBackgroundResource(R.drawable.btn_vibor_pressed);

                                l1_1_27.setVisibility(View.GONE);
                                l1_1_30.setVisibility(View.GONE);
                                counterSecondLine = 1;
                            }

                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                    button4.setVisibility(View.VISIBLE); button4.startAnimation(a);
button4.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        try {  if (counterSecondLine==0) {
            button4.setBackgroundResource(R.drawable.btn_vibor_pressed);
            counterSecondLine = 2;

            l1_1_26.setVisibility(View.GONE);
            l1_1_27.setVisibility(View.GONE);
            l1_1_28.setVisibility(View.GONE);
            l1_1_29.setVisibility(View.GONE);

        }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
});

break;
                case 28:  l1_1_26.setVisibility(View.VISIBLE); l1_1_26.startAnimation(a); break;
                case 29:      l1_1_27.setVisibility(View.VISIBLE); l1_1_27.startAnimation(a); break;
                case 30:      l1_1_28.setVisibility(View.VISIBLE); l1_1_28.startAnimation(a); break;
                case 31:    l1_1_29.setVisibility(View.VISIBLE); l1_1_29.startAnimation(a); break;
                case 32:    l1_1_30.setVisibility(View.VISIBLE); l1_1_30.startAnimation(a); break;
                case 33:   l1_1_31.setVisibility(View.VISIBLE); l1_1_31.startAnimation(a); break;
                case 34:   l1_1_32.setVisibility(View.VISIBLE); l1_1_32.startAnimation(a); break;
                case 35:   l1_1_33.setVisibility(View.VISIBLE); l1_1_33.startAnimation(a); break;
                case 36:   l1_1_34.setVisibility(View.VISIBLE); l1_1_34.startAnimation(a); break;
                case 37:   l1_1_35.setVisibility(View.VISIBLE); l1_1_35.startAnimation(a); break;
                case 38:   l1_1_36.setVisibility(View.VISIBLE); l1_1_36.startAnimation(a); break;
                case 39:   l1_1_37.setVisibility(View.VISIBLE); l1_1_37.startAnimation(a); break;
                case 40:   l1_1_38.setVisibility(View.VISIBLE); l1_1_38.startAnimation(a); break;
                case 41:  l1_1_39.setVisibility(View.VISIBLE); l1_1_39.startAnimation(a); break;
                case 42:  l1_1_40.setVisibility(View.VISIBLE); l1_1_40.startAnimation(a); break;
                case 43:    l1_1_41.setVisibility(View.VISIBLE); l1_1_41.startAnimation(a); break;
                case 44:    l1_1_42.setVisibility(View.VISIBLE); l1_1_42.startAnimation(a); break;
                case 45:      l1_1_43.setVisibility(View.VISIBLE); l1_1_43.startAnimation(a); break;
                case 46:        l1_1_44.setVisibility(View.VISIBLE); l1_1_44.startAnimation(a); break;

                case 47: button5.setVisibility(View.VISIBLE); button5.startAnimation(a);
                button5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (counterThirdLine==0) {
                                button5.setBackgroundResource(R.drawable.btn_vibor_pressed);
                                counterThirdLine=1;
                                line=48;
                            }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                    button6.setVisibility(View.VISIBLE); button6.startAnimation(a);
                    button6.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if (counterThirdLine==0) {
                                    button6.setBackgroundResource(R.drawable.btn_vibor_pressed);
                                    l1_1_45.setVisibility(View.GONE);
                                    l1_1_46.setVisibility(View.GONE);
                                    l1_1_47.setVisibility(View.GONE);
                                    l1_1_48.setVisibility(View.GONE);
                                    counterThirdLine=2;
                                    line=52;
                                }
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

break;
                case 48:   l1_1_45.setVisibility(View.VISIBLE); l1_1_45.startAnimation(a); break;
                case 49:    l1_1_46.setVisibility(View.VISIBLE); l1_1_46.startAnimation(a); break;
                case 50:    l1_1_47.setVisibility(View.VISIBLE); l1_1_47.startAnimation(a); break;
                case 51:    l1_1_48.setVisibility(View.VISIBLE); l1_1_48.startAnimation(a); break;
                case 52:    l1_1_49.setVisibility(View.VISIBLE); l1_1_49.startAnimation(a); break; //dnjhjq
                case 53:    l1_1_50.setVisibility(View.VISIBLE); l1_1_50.startAnimation(a); break;
                case 54:    l1_1_51.setVisibility(View.VISIBLE); l1_1_51.startAnimation(a); break;
                case 55:     l1_1_52.setVisibility(View.VISIBLE); l1_1_52.startAnimation(a); break;
                case 56:   l1_1_53.setVisibility(View.VISIBLE); l1_1_53.startAnimation(a); break;
                case 57:  l1_1_54.setVisibility(View.VISIBLE); l1_1_54.startAnimation(a); break;
                case 58:  l1_1_55.setVisibility(View.VISIBLE); l1_1_55.startAnimation(a); break;
                case 59:  l1_1_56.setVisibility(View.VISIBLE); l1_1_56.startAnimation(a); break;
                case 60:  l1_1_57.setVisibility(View.VISIBLE); l1_1_57.startAnimation(a); break;

                case 61:   l1_1_58.setVisibility(View.VISIBLE); l1_1_58.startAnimation(a);
                    harry.setVisibility(View.VISIBLE); harry.startAnimation(a); break;

                case 62:    l1_1_59.setVisibility(View.VISIBLE); l1_1_59.startAnimation(a); break;
                case 63:    l1_1_60.setVisibility(View.VISIBLE); l1_1_60.startAnimation(a); break;
                case 64:   l1_1_61.setVisibility(View.VISIBLE); l1_1_61.startAnimation(a); break;
                case 65:  l1_1_62.setVisibility(View.VISIBLE); l1_1_62.startAnimation(a); break;
                case 66: l1_1_63.setVisibility(View.VISIBLE); l1_1_63.startAnimation(a); break;
                case 67: l1_1_64.setVisibility(View.VISIBLE); l1_1_64.startAnimation(a); break;

                case 68:  l1_1_65.setVisibility(View.VISIBLE); l1_1_65.startAnimation(a); break;
                case 69:  l1_1_66.setVisibility(View.VISIBLE); l1_1_66.startAnimation(a); break;
                case 70:  l1_1_67.setVisibility(View.VISIBLE); l1_1_67.startAnimation(a); break;
                case 71:  l1_1_68.setVisibility(View.VISIBLE); l1_1_68.startAnimation(a); break;
                case 72:  l1_1_69.setVisibility(View.VISIBLE); l1_1_69.startAnimation(a); break;
                case 73: l1_1_70.setVisibility(View.VISIBLE); l1_1_70.startAnimation(a); break;

                case 74: l1_1_71.setVisibility(View.VISIBLE); l1_1_71.startAnimation(a); break;
                case 75: l1_1_72.setVisibility(View.VISIBLE); l1_1_72.startAnimation(a); break;
                case 76: button7.setVisibility(View.VISIBLE); button7.startAnimation(a);
button7.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        try {
            button7.setBackgroundResource(R.drawable.btn_vibor_pressed);
            Intent prev = new Intent(Level1_1.this, Preview.class);
            startActivity(prev);
            finish();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
});


                default:break;

            }



         //   super.onProgressUpdate(values);
        }
    }
}