package com.ggs.harryandrationalthinking.levels.Level3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.ggs.harryandrationalthinking.R;
import com.ggs.harryandrationalthinking.levels.level2.Level2;
import com.ggs.harryandrationalthinking.levels.level2.Preview;

public class Preview_lvl3 extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGHT = 3500;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_lvl3);
        new Handler().postDelayed(new Runnable() {

            @Override

            public void run() {

                Intent mainIntent = new Intent(Preview_lvl3.this, Level3.class);

                Preview_lvl3.this.startActivity(mainIntent);

                Preview_lvl3.this.finish();

            }

        }, SPLASH_DISPLAY_LENGHT);

    }



    @Override

    public void onBackPressed() {

        super.onBackPressed();

    }

}